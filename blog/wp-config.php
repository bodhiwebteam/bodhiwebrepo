<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bodhiinf_blog');

/** MySQL database username */
define('DB_USER', 'bodhiinf_blogadm');

/** MySQL database password */
define('DB_PASSWORD', 'fqG7bTMT)(.9');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Co%8VnHj|[|=^hBHlqko:g,Du;K{@fJV.r*Bsc$vHOX61*#%3Uu7b70A]d5R8=OX');
define('SECURE_AUTH_KEY',  '}vyX~s>d&!>*b42A!_X.e$h$%4gSOnv1vm?tJUxwXMuP<>A7oEJmg[)Irq_<$~{&');
define('LOGGED_IN_KEY',    '4|%1sXG6M#`E)$xFhCQxQNAvd<w{K;A>YWm7,5oYsm+fh+MH6u]>LLX}8uQfjUAl');
define('NONCE_KEY',        'TfT,J`-0Ll*Tibr+Z n<d7V&gp)14Xg!nE/Ai~TmT(`m5;0LKJ2t$](SE}yHN{&v');
define('AUTH_SALT',        ']`s(a rzDF=GoYjOsQYlBAi+, FfcDW0hfa9r3}s0@Q%8+.17~|1u4!qqGIZIt*+');
define('SECURE_AUTH_SALT', '`qtUzK]qr7a6#<!ni})@m48vy$tg~|$boKG5~W+]!P^x-b$8~ZhgmY&*2q(^%U6t');
define('LOGGED_IN_SALT',   '_iaUto%{FS3=.n;D.sO<f7U}YT!&>cXl6uwYSpyiPlicd%U2HR1QaBTu5R;gUvoB');
define('NONCE_SALT',       'uWoILn:aYUP>aW1]/`SR2?m4.nkC4vyl$x,=C0sbzK0%&  P:NIo.+nK1A9x3SYH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
