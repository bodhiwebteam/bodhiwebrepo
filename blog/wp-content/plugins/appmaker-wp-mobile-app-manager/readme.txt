=== Appmaker WP Mobile App Manager ===

Tags: appmaker,wordpress,mobile,native,app,android,ios,appmaker.xyz
Requires at least: 4.4
Tested up to: 4.5.2
Stable tag: 0.0.33
License: GPLv2 or later
Contributors: Appmaker.xyz
Connect WordPress site to Android and iOS mobile app created from Appmaker.xyz
== Description ==

**This Plugin is used to connect your WordPress site to Android and iOS mobile app created from  [Appmaker.xyz Native WordPress App Creator](https://appmaker.xyz/wordpress/)**

Got any questions or need any help? Email us at [support@appmaker.xyz](mailto:support@appmaker.xyz).

== Installation ==

1. Create app for your WordPress site from https://appmaker.xyz/wordpress
2. Copy API KEY & API SECRET from Appmaker
3. Install this plugin on your WordPress site
4. Enter API KEY & API SECRET copied from appmaker in plugin settings
5. Your app is ready to use ,You can now download/manage app from https://appmaker.xyz/dashboard

== Frequently Asked Questions ==

= Do you upload the app to store? =

No. We provide signed APK and IPA files which you need to upload to store using your developer account.
== Changelog ==

= 0.0.1 =
* Beta Release
