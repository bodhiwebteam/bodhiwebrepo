<?php

/**
 * Class APPMAKER_WP_CONVERTER
 */
class APPMAKER_WP_Converter {
	static $key = 1;

	public static function convert_inAppPage_data( $data_obj, $key ) {
		$app_value            = array();
		$app_value['id']      = $key;
		$app_value['title']   = html_entity_decode( $data_obj->title );
		$app_value['widgets'] = array();

		if ( class_exists('APPMAKER_WC') && ! isset( APPMAKER_WC::$api->APPMAKER_WC_REST_Products_Controller ) ) {
			$product_controller = new APPMAKER_WC_REST_Products_Controller();
		} elseif(class_exists('APPMAKER_WC')) {
			$product_controller = APPMAKER_WC::$api->APPMAKER_WC_REST_Products_Controller;
		}

		foreach ( $data_obj->widgets as $value_obj ) {
			$skip = false;

			$value  = clone( $value_obj );
			$widget = array(
				'type'  => $value->type,
				'title' => $value->title,
				'data'  => array(),
			);
			if ( 'pre_build_product_scroller' === $widget['type'] ) {
				switch ( $value->data[0]->data->action_value->value ) {
					case 'FEATURED':
						$value->data = APPMAKER_WC_Helper::wc_get_featured_product_ids();
						break;
					case 'SALE':
						$value->data = APPMAKER_WC_Helper::wc_get_product_ids_on_sale();
						break;
					default:
						$value->data = array();
				}
				if ( empty( $value->data ) ) {
					$skip = true;
				} else {
					shuffle( $value->data );
					$value->data = array_slice( $value->data, 0, 15 );
				}
			}
			foreach ( $value->data as $value_data ) {
				$data = false;
				if ( 'product_scroller' === $widget['type'] ) {
					if ( ! isset( $value_data->data->action_value->value ) && ( is_string( $value_data ) || is_numeric( $value_data ) ) ) {
						$product = wc_get_product( $value_data );
					} else {
						$product = wc_get_product( $value_data->data->action_value->value );
					}
					if ( ! empty( $product ) ) {
						$data = $product_controller->get_product_data( $product );
					}
				} elseif ( 'pre_build_product_scroller' === $widget['type'] ) {
					$product = wc_get_product( $value_data );
					if ( ! empty( $product ) ) {
						$data = $product_controller->get_product_data( $product );
					}
				} elseif ( 'html' === $widget['type'] ) {
					$data['html'] = wpautop( do_shortcode( $value_data->data->html->value ) );
				} else {
					$data = array(
						'image'  => self::getValue( $value_data->data->image ),
						'title'  => self::getValue( $value_data->data->title ),
						'action' => array(
							'type'   => $value_data->data->action->value,
							'params' => self::get_params( $value_data->data->action->value, $value_data->data->action_value->value ),
						),
					);
				}
				if ( false !== $data ) {
					$widget['data'][] = $data;
				}
			}
			if ( 'pre_build_product_scroller' === $widget['type'] ) {
				$widget['type'] = 'product_scroller';
			}
			if ( true !== $skip && ! empty( $widget['data'] ) ) {
				$app_value['widgets'][] = $widget;
			}
		}
		$app_value['hash'] = md5( serialize( $app_value ) );

		return ( $app_value );
	}

	/**
	 * @param mixed $value
	 *
	 * @return string
	 */
	protected static function getValue( $value ) {
		if ( is_string( $value ) ) {
			return $value;
		} else {
			return $value->value;
		}
	}

	public static function get_params( $action, $action_value ) {
		switch ( $action ) {
			case 'OPEN_URL' :
			case 'OPEN_IN_WEB_VIEW' : {
				$params = array( 'url' => $action_value );
				break;
			}
			case 'LIST_POST' : {
				$params = array( 'categories' => $action_value );
				break;

			}
			case 'LIST_PRODUCT' : {
				$params = array( 'category' => $action_value );
				break;
			}
			case 'OPEN_IN_APP_PAGE' : {
				$params = array( 'id' => $action_value );
				break;

			}
			case 'OPEN_POST' : {
				$params = array( 'id' => $action_value );
				break;

			}
			case 'OPEN_PRODUCT' : {
				$params = array( 'id' => $action_value );
				break;
			}
			default : {
				$params = array();
				break;

			}
		}

		return $params;
	}

	public static function convert_navMenu_data( $data ) {
		$app_value         = array();
		$app_value['data'] = self::convert_navMenu_data_item( $data );
		$app_value['hash'] = md5( serialize( $app_value ) );

		return ( $app_value );
	}

	public static function convert_navMenu_data_item( $data ) {
		$return = array();
		foreach ( $data as $value ) {
			if(isset($value->children)){
				$nodes = $value->children;
			} elseif (isset($value->nodes)){
				$nodes = $value->nodes;
			}else{
				$nodes = array();
			}

			$menu = array(
				'id'     => self::$key ++,
				'title'  => html_entity_decode( $value->title ),
				'icon'   => self::getValue( $value->icon ),
				'type'   => isset($value->type->value)?$value->type->value:$value->type->id,
				'action' => array(),
				'nodes'  => self::convert_navMenu_data_item( $nodes ),
			);
			if ( 'title' !== $menu['type'] ) {
				if ( is_string( $value->action_value ) ) {
					$val = $value->action_value;
				} else {
					$val = isset($value->action_value->value)?$value->action_value->value:$value->action_value->id;
				}
				$menu['action'] = array(
					'type'   => isset($value->action->value)?$value->action->value:$value->action->id,
					'params' => self::get_params( isset($value->action->value)?$value->action->value:$value->action->id, $val ),
				);
			}
			$return[] = $menu;
		}

		return $return;
	}
}
