<?php

/**
 * Class APPMAKER_WP_API
 *
 * @property APPMAKER_WP_REST_Posts_Controller $APPMAKER_WP_REST_Posts_Controller
 * @property APPMAKER_WP_REST_Posts_Controller $APPMAKER_WP_REST_Pages_Controller
 * @property APPMAKER_WP_REST_Terms_Controller $APPMAKER_WP_REST_Category_Controller
 * @property APPMAKER_WP_REST_Terms_Controller $APPMAKER_WP_REST_Tag_Controller
 * @property APPMAKER_WP_REST_BACKEND_INAPPPAGE_Controller $APPMAKER_WP_REST_BACKEND_INAPPPAGE_Controller
 * @property APPMAKER_WP_REST_BACKEND_NAV_Controller $APPMAKER_WP_REST_BACKEND_NAV_Controller"
 * @property APPMAKER_WP_REST_BACKEND_MEDIA_Controller $APPMAKER_WP_REST_BACKEND_MEDIA_Controller
 * @property APPMAKER_WP_REST_BACKEND_Posts_Controller $APPMAKER_WP_REST_BACKEND_Posts_Controller
 * @property APPMAKER_WP_REST_BACKEND_Terms_Controller $APPMAKER_WP_REST_BACKEND_Terms_Controller
 * @property APPMAKER_WP_REST_FRONTEND_Controller $APPMAKER_WP_REST_FRONTEND_INAPPPAGE_Controller
 * @property APPMAKER_WP_REST_FRONTEND_Controller $APPMAKER_WP_REST_FRONTEND_NAV_Controller
 */
class APPMAKER_WP_API {
	public static $_instance;
	public $settings_key;

	public function __construct() {

		// WP REST API.
		$this->rest_api_init();
	}

	private function rest_api_init() {
		global $wp_version;

		// REST API was included starting WordPress 4.4.
		if ( version_compare( $wp_version, 4.4, '<' ) ) {
			return;
		}

		$this->rest_api_includes();

		// Init REST API routes.
		add_action( 'rest_api_init', array( $this, 'register_rest_routes' ) );
	}

	public function rest_api_includes() {

		// WP-API classes and functions.
		if ( ! class_exists( 'WP_REST_Controller' ) ) {
			include_once( 'vendor/wp-rest-functions.php' );
			include_once( 'vendor/class-wp-rest-controller.php' );
		}

		if ( ! class_exists( "APPMAKER_WP_REST_Controller" ) ) {
			// Abstract Classes
			include_once( 'abstracts/abstract-appmaker-wp-rest-controller.php' );

			// Endpoints Classes
			include_once( 'endpoints/class-appmaker-wp-rest-posts-controller.php' );
			include_once( 'endpoints/class-appmaker-wp-rest-terms-controller.php' );
			include_once( 'endpoints/appmaker/class-appmaker-wp-rest-backend-media-controller.php' );
			include_once( 'endpoints/appmaker/class-appmaker-wp-rest-backend-inapppage-controller.php' );
			include_once( 'endpoints/appmaker/class-appmaker-wp-rest-frontend-controller.php' );
			include_once( 'endpoints/appmaker/class-appmaker-wp-rest-backend-posts-controller.php' );
			include_once( 'endpoints/appmaker/class-appmaker-wp-rest-backend-terms-controller.php' );
			include_once( 'endpoints/appmaker/class-appmaker-wp-rest-backend-nav-controller.php' );

			// Format Converter
			include_once( 'class-appmaker-wp-converter.php' );
		}
		include_once( 'endpoints/class-appmaker-wp-rest-posts-controller.php' );
		include_once( 'endpoints/class-appmaker-wp-rest-terms-controller.php' );

	}

	/**
	 * @return APPMAKER_WP_API
	 */
	public static function get_instance() {
		if ( ! is_a( self::$_instance, "APPMAKER_WP_API" ) ) {
			self::$_instance = new APPMAKER_WP_API();
		}

		return self::$_instance;
	}

	public function register_rest_routes() {
		global $wp_post_types;
		global $wp_taxonomies;

		if ( isset( $wp_post_types['post'] ) ) {
			$wp_post_types['post']->show_in_rest = true;
			$wp_post_types['post']->rest_base    = 'posts';
		}

		if ( isset( $wp_post_types['page'] ) ) {
			$wp_post_types['page']->show_in_rest = true;
			$wp_post_types['page']->rest_base    = 'pages';
		}

		if ( isset( $wp_taxonomies['category'] ) ) {
			$wp_taxonomies['category']->show_in_rest = true;
			$wp_taxonomies['category']->rest_base    = 'categories';
		}

		if ( isset( $wp_taxonomies['post_tag'] ) ) {
			$wp_taxonomies['post_tag']->show_in_rest = true;
			$wp_taxonomies['post_tag']->rest_base    = 'tags';
		}


		$controllers = array(
			"APPMAKER_WP_REST_Posts_Controller"              => array( "APPMAKER_WP_REST_Posts_Controller", "post" ),
			"APPMAKER_WP_REST_Pages_Controller"              => array( "APPMAKER_WP_REST_Posts_Controller", "page" ),
			"APPMAKER_WP_REST_Category_Controller"           => array(
				"APPMAKER_WP_REST_Terms_Controller",
				"category"
			),
			"APPMAKER_WP_REST_Tag_Controller"                => array(
				"APPMAKER_WP_REST_Terms_Controller",
				"post_tag"
			),
			"APPMAKER_WP_REST_BACKEND_INAPPPAGE_Controller",
			"APPMAKER_WP_REST_BACKEND_NAV_Controller",
			"APPMAKER_WP_REST_BACKEND_MEDIA_Controller",
			"APPMAKER_WP_REST_BACKEND_Posts_Controller",
			"APPMAKER_WP_REST_BACKEND_Terms_Controller",
			"APPMAKER_WP_REST_FRONTEND_INAPPPAGE_Controller" => array(
				"APPMAKER_WP_REST_FRONTEND_Controller",
				"inAppPages"
			),
			"APPMAKER_WP_REST_FRONTEND_NAV_Controller"       => array(
				"APPMAKER_WP_REST_FRONTEND_Controller",
				"navigationMenu"
			)
		);

		foreach ( $controllers as $key => $controller ) {
			if ( is_array( $controller ) ) {
				$this->{$key} = new $controller[0]( $controller[1] );
				$this->$key->register_routes();
			} else {
				$this->$controller = new $controller();
				$this->$controller->register_routes();
			}
		}
	}
}