<?php
/*
Plugin Name: Appmaker WP Mobile App Manager
Plugin URI: https://appmaker.xyz
Description: Appmaker Mobile App Manager.
Version: 0.0.33
Author: Appmaker
Author URI: https://appmaker.xyz
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once dirname( __FILE__ ) . '/lib/class-appmaker-wp-options.php';

/**
 * Class APPMAKER_WP
 *
 */
class APPMAKER_WP {
	static $root;
	/** @var  APPMAKER_WP_API */
	static $api;

	public static function init() {
		register_activation_hook( __FILE__, self::func( "activated" ) );
		register_deactivation_hook( __FILE__, self::func( 'deactivate' ) );
		register_uninstall_hook( __FILE__, self::func( 'uninstall' ) );
		add_action( 'plugins_loaded', self::func( 'init_plugins_loaded' ) );
		defined( 'APPMAKER_WP_API' ) || define( 'APPMAKER_WP_API', true );
	}

	public static function func( $func ) {
		return array( self::name(), $func );
	}

	public static function name() {
		return 'APPMAKER_WP';
	}

	public static function init_plugins_loaded() {

		if ( ! self::compatible_version() ) {
			return;
		}

		if ( is_admin() ) {
			new APPMAKER_WP_Options();
		}


		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), self::func( 'add_plugin_action_links' ) );
		self::$root = dirname( __FILE__ );
		self::load_appmaker_rest_api();

	}

	public static function compatible_version() {
		if ( version_compare( $GLOBALS['wp_version'], '4.4', '<' ) ) {
			return false;
		}

		return true;
	}

	public static function load_appmaker_rest_api() {
		if ( ! class_exists( "APPMAKER_WP_API" ) ) {
			include_once( self::$root . '/lib/class-appmaker-wp-api.php' );
		}
		self::$api = APPMAKER_WP_API::get_instance();
	}

	/**
	 * Registers default REST API routes.
	 *
	 * @since 4.4.0
	 */
	public static function create_initial_rest_routes() {

	}

	public static function add_plugin_action_links( $links ) {
		return array_merge(
			array(
				'settings'   => '<a href="' . get_bloginfo( 'wpurl' ) . '/wp-admin/options-general.php?page=appmaker-wp-admin">Settings</a>',
				// 'docs' => '<a target="_blank" href="https://appmaker.xyz/wordpress/docs/">Docs</a>',
				'create_app' => '<a target="_blank" href="https://appmaker.xyz/wordpress/">Create App</a>'
			),
			$links
		);
	}

	public static function disabled_notice() {
		$class   = 'notice notice-error';
		$message = __( 'Appmaker WP Mobile App Manager WordPress 4.4 or higher', 'appmaker_wp' );
		printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );

	}

	public static function activated() {
		if ( ! self::compatible_version() ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );
			add_action( 'admin_notices', self::func( 'disabled_notice' ) );
			wp_die( __( 'Appmaker Mobile App Manager requires WordPress 4.4 or higher. Contact support@appmaker.xyz for more information', 'appmaker_wp' ) );
		}
		do_action( "appmaker_wp_plugin_activate" );
	}

	public static function uninstall() {
		//   flush_rewrite_rules();
		do_action( "appmaker_wp_plugin_uninstall" );
	}

	public static function deactivate() {
		do_action( "appmaker_wp_plugin_deactivate" );
	}

}

APPMAKER_WP::init();