<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 footer_col_1_wrap">
                <div class="footer_col1">
                    <div class="footer_logo">
                        <img src="<?php bloginfo('template_url'); ?>/images/footer_logo.png" alt="">
                    </div>
                    <div class="footer_social">
                        <a target="_blank" href="https://www.facebook.com/pages/Bodhi-info-solutions/293809530773903" title="facebook"><span><i class="fa fa-facebook-f"></i></span></a>
                        <a target="_blank" href="https://www.linkedin.com/company/bodhi-info-solutions"><span><i class="fa fa-linkedin" title="linkedin"></i></span></a>
                        <a target="_blank" href="https://plus.google.com/115280885518776516760/about?hl=en" title="google pluse"><span><i class="fa fa-google-plus"></i></span></a>

                    </div>
                    <div class="footer_broch_lnks">
                        <a href="http://bodhiinfo.com/bodhi_brochure.pdf" title="Brochure" target="_blank"><i class="ion ion-android-download"></i>Brochure</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_col2">
                    <div class="footer_projection">
                      <ul>
                        <li>
                            <div style="display: inline-flex;" class="head_for_footer"><div id="count-test-2" class="demo"></div>+</div>
                            <div class="footer_sub_head">Developed Projects</div>
                        </li>
                        <li>
                            <div style="display: inline-flex;" class="head_for_footer"><div id="count-test-3" class="demo"></div>%</div>
                            <div class="footer_sub_head">Dedicated Team</div>
                        </li>
                        <li>
                            <div style="display: inline-flex;" class="head_for_footer"><span>00</span><div id="count-test-4" class="demo"></div>+</div>
                            <div class="footer_sub_head">Countries</div>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_contact">
                    <h4>Reach Us</h4>
                    <p>Kuniyil Kavu Junction,<br />
                        Jafferkhan Colony Road,<br>
                        Calicut - 673001<br />Kerala</p>
                    <hr>
                    <div class="email_row"><span><i class="fa fa-envelope"></i></span><a href="mailto:mail@bodhiinfo.com" title="mail@bodhiinfo.com">  mail@bodhiinfo.com </a><br></div>
                    <br>
                    <div class="number_row"><span><i class="fa fa-phone"></i></span><a href="tel:+9104954019587" title="0495 4019587">  0495 4019587 </a><br></div>
                    <div class="number_row"><span><i class="fa fa-mobile"></i></span><a href="tel:+918086450007" title="08086 450007">  08086 450007</a></div>

                </div>
            </div>
        </div>
        <hr style="border-top:1px solid #425355; margin-bottom:0; margin-top:50px;">
        <p class="copy">Copyright © <?= date('Y'); ?> Bodhi. All Rights Reserved.</p>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>