<div class="about_wrap">
    <div class="container">
        <div class="cat_page_hd">
            <h3>Privacy Policy</h3>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="about_wrap">
                    <p>Suprabhaatham Online and its websites owned by Kozhikode Iqrau Publications is an online news media, which is constantly striving to provide services to facilitate its users. Since we are committed to your right to privacy, we have set privacy statement based on the information we have obtained from you. This document, the Privacy policies is subjected to Terms of Service of Iqrau Publications.</p>
                    <h4>Right to use the site</h4>
                    <p>You can use the site at no charge, and we will not collect any personal information belonging to the user without your consent. In the event of any Information collected from our users and or visitors who use the services offered by the site, as subscriber then the information we collect and as required by law may include - but not necessarily are limited to - your email address, first name, last name (family name), and the user's password, mailing address, the country code, phone number or fax. If any of our service is paid or premium service then, we may collect additional information, including billing address, credit card number, expiration date of the credit card and tracking information from checks or money orders.</p>
                    <p>How the site uses the information it collects or tracks? </p>
                    <p>Iqrau Publications collects information from its members in the first place to make sure we are able to fulfill your requirements and provide a distinctive personal experience.</p>
                    <p>Sharing of the information collected with others?</p>
                    <p>The information we have collected will not be shared with any individual or organization without the consent of the subscriber.</p>
                    <p>Iqrau Publications does not sell, or lease, or lend any personal information on an individual level, to any third party. And we give our utmost care and full confidentiality for the information given by you. We also are committed to full cooperation in the event of any circumstance be demanding by the law or any legal action to provide information about the subscriber.</p>
                    <h4>DISCLAIMER</h4>
                    <p>WE may make changes to the Privacy Policy subject to our Terms of Service published in our site, from time to time as result of a change in the policy of our company or on the basis of laws or new regulation. Thus we own the right to change the Privacy Policy at any time without notice. The user is advised to be updated with the latest privacy policy published from time to time. By using our services, user (you) agree to our terms of use and our privacy policy.</p>
                    <p>We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.</p>
                    <p>If you have any queries regarding this privacy statement, please contact us at the following mail address web@suprabhaatham.com.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="other_news_block">
                    <h3 style="margin-top: 0px;">Latest News</h3>
                    <div class="other_news_inner">
                        <ul>
                            <?php
                            $latestArgs = array(
                                'category' => 7,
                                'numberposts' => 5,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_status' => 'publish'
                            );
                            $latestPosts = get_posts($latestArgs);
                            foreach($latestPosts as $latestPost)
                            {
                                ?>
                                <li><a href="<?= get_permalink($latestPost -> ID) ?>"><?= $latestPost -> post_title; ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="social_face">
                        <h3 class="soc_face_head">Social Face</h3>
                        <div class="social_face_tabs">
                            <!--<span class="active" data-target="#fb_soc"><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt="" /></span>
                            <span data-target="#tw_soc"><img src="<?php bloginfo('template_url'); ?>/images/tw.png" alt="" /></span>-->
                            <div class="social_face_tabs" style="display: block;">
                                <button class="btn btn-primary active" data-target="#fb_soc">Facebook</button>
                                <button class="btn btn-primary" data-target="#tw_soc">Twitter</button>
                            </div>
                            <div class="bd_clear"></div>
                        </div>
                        <div class="social_face_inner active" id="fb_soc">
                            <div class="fb-page" data-href="https://www.facebook.com/Suprabhaatham/" data-tabs="timeline" data-height="490" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Suprabhaatham/"><a href="https://www.facebook.com/Suprabhaatham/">Suprabhaatham Daily</a></blockquote></div></div>
                        </div>
                        <div class="social_face_inner" id="tw_soc">
                            <a class="twitter-timeline"  href="https://twitter.com/Suprabhaatham" data-widget-id="674570918199341056">Tweets by @Suprabhaatham</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>