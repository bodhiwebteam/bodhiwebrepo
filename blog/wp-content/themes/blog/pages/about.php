<div class="about_wrap">
    <div class="container">
        <div class="cat_page_hd">
            <h3>About Us</h3>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="about_wrap">
                    <p class="first">Yes, it is true that Suprabhaatham put a land mark in the history of Indian Journalism. No newspaper in India, except Suprabhaatham, has started publishing with six editions and six lakhs copies in the very first day. There are a lot of media establishments in Kerala including print media. All of the newspapers started with very few copies and achieved prosperity only after a struggle of long years. But, with the grace of the Almighty we could take off easily by achieving the third largest newspaper in Malayalam.</p>
                    <p>Though, Suprabhaatham is only an infant in Malayalam journalism, the Enlighted readers admit that it has acquired maturity by birth and set new trends in Malayalam journalism within a short period. We are trying to restructure the way of news presentation. We believe in change according to the need of the reading community.</p>
                    <p>At the same time we are against the violation of ethics in journalism. News should be factual. No journalist or news paper establishment has the right to distort real news to appease any section. We understand that journalism should be away from partialism. So, the goal of Suprabhaatham, is to establish an unbiased news culture.</p>
                    <p>As mentioned above, Suprabhatam is a new comer in Malayalam journalism. It started publication on 1st September 2014. It was inaugurated by the Honourable Chief Minister Oommen Chandy. Honourable ministers like Ramesh Chennithala, P.K Kunhalikkutty, K.C Joseph, P.K Abdurub, Dr. M.K Muneer and dignitaries like Panakkad Sayyid Hyder Ali Shihab Thangal, Kodiyeri Balakrishnan, E. Ahmed, Abdussamad Samadani, Thomas Jacob etc had reached to bless the new-born news baby.</p>
                    <p>Suprabhaatham is the dream project of Iqra Publications, a unit of the great organisation, Samatha Kerala Jam Iyyathul Ulama. Samastha a muslim organization which is fully engaged in the development of educational, cultural and moral activities of people, entered in anew venture in the Media titled as Suprabhaatham Daily. Samastha Kerala Jam Iyyathul Ulama formed in 1926 at Kozhikkode, under the leadreship of great Scholar Varakkal Mullakkoya Thangal. Samastha roots in all over the villages in Kerala, other states in India and also in abroad. Samastha is the supreme religious and organizational commanding and decision making body of more than 85% of the Muslims in Kerala. More than 1200000 (twelve lakhs) of students are studying in 10000 Madrassas in the state. As per the latest intimation the Madrassa Teachers limits are exceeded to more than 100000, Apart from this 30000 Masjids are existing in various locations in the state and abroad.</p>
                    <p>Anakkara C Koya Kutty Musliyar and Cherussery Zainudheen Musliyar are the current President and General Secretary of Samastha Kerala Jamiyyathul Ulama respectively.
                        The desire of the leaders of Samastha is to establish a novel news culture with high moral values. Being a product of Samastha, Suprabhaatham is against all anti social activities like corruption, alcoholism, bribe etc. Our motto is not to dig money, but to serve the society and safeguard the good values in the society.</p>
                    The Cheif Patron of Suprabhaatham daily is Panakkad Sayyid Hyderali Shihab Thangal and Chairman is Janab Kottumala TM Bappu musliyar and other leaders of Iqrah publications are MT Abdulla Musliyar, Prof. K Alikkutty Musliyar and CKM Sadiq Musliyar (Vice Chairman) Abdul Hameed Faisy Ambalakkadavu (Gen. Convenor) U Shafi Haji (Convenor) and Panakkad Hameed Ali Shihab Thangal (Tressurer).
                    <p>Suprabhaatham has a well experienced team of editorial staff lead by Mr. Navas Poonoor (Managing editor) and A. Sajeevan (Executive Editor). Former AIR director C.P Rajasekharan had served as Associate Editor in the begginning. Musthafa Mundupara and IM Abdu Rahiman are the CEO and Deputy CEO of Suprabhaatham daily respectively.</p>
                    <p>Suprabhaatham has editions in Thiruvananthapuram, Kochi, Thrissur, Malappuram, Kozhikode and Kannur districts and a corporate office in Kozhikode. We are proud that we could start an online edition in the same time of the launching of Suprabhaatham daily.</p>
                    <p>Our aim is to become the leader of Malayalam newspaper industry with number one in Circulation. To attain the goal we have planned to start editions in Gulf countries in the immediate future.</p>
                    <p>Pray for us to achieve our wishes......</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="other_news_block">
                    <h3 style="margin-top: 0px;">Latest News</h3>
                    <div class="other_news_inner">
                        <ul>
                            <?php
                            $latestArgs = array(
                                'category' => 7,
                                'numberposts' => 5,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_status' => 'publish'
                            );
                            $latestPosts = get_posts($latestArgs);
                            foreach($latestPosts as $latestPost)
                            {
                                ?>
                                <li><a href="<?= get_permalink($latestPost -> ID) ?>"><?= $latestPost -> post_title; ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="social_face">
                        <h3 class="soc_face_head">Social Face</h3>
                        <div class="social_face_tabs">
                            <!--<span class="active" data-target="#fb_soc"><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt="" /></span>
                            <span data-target="#tw_soc"><img src="<?php bloginfo('template_url'); ?>/images/tw.png" alt="" /></span>-->
                            <div class="social_face_tabs" style="display: block;">
                                <button class="btn btn-primary active" data-target="#fb_soc">Facebook</button>
                                <button class="btn btn-primary" data-target="#tw_soc">Twitter</button>
                            </div>
                            <div class="bd_clear"></div>
                        </div>
                        <div class="social_face_inner active" id="fb_soc">
                            <div class="fb-page" data-href="https://www.facebook.com/Suprabhaatham/" data-tabs="timeline" data-height="490" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Suprabhaatham/"><a href="https://www.facebook.com/Suprabhaatham/">Suprabhaatham Daily</a></blockquote></div></div>
                        </div>
                        <div class="social_face_inner" id="tw_soc">
                            <a class="twitter-timeline"  href="https://twitter.com/Suprabhaatham" data-widget-id="674570918199341056">Tweets by @Suprabhaatham</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>