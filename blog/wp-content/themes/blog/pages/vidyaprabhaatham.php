 <?php get_header(); ?>
 <!-- Content -->        
    <div class="inner_wrap">
        <div class="container">
        	<div class="row">
        		<div class="col-lg-12">
        			<div class="vidya_sp_wrap">
        				<div class="row">
        					<div class="col-lg-4 col-md-4 col-sm-4">
        						<?= vidyaBanner(); ?>
        					</div>
        					<div class="col-lg-2 col-md-2 col-sm-2">
        						<a class="vidya_btn vidya_btn_square" style="background-color: #09787f;" href="<?= site_url(); ?>/game-zone"><div class="vidya_btn_ico"><i class="fa fa-gamepad"></i></div>Game Zone</a>
        						<a class="vidya_btn vidya_btn_long" style="background-color: #4e4848;" href="<?= site_url(); ?>/art"><div class="vidya_btn_ico"><i class="fa fa-paint-brush"></i></div>Art Zone</a>
        					</div>
        					<div class="col-lg-4 col-md-4 col-sm-4">
        						<a href="<?= site_url(); ?>/category/project-helper" class="vidya_btn" style="background-color: #ef9646;"><div class="vidya_btn_ico"><i class="fa fa-file-text"></i></div>Project Helper</a>
        						<div class="row">
        							<div class="col-lg-6 col-md-6 col-sm-6">
        								<a href="<?= site_url(); ?>/category/edu-videos" class="vidya_btn vidya_btn_square" style="background-color: #67a901;"><div class="vidya_btn_ico"><i class="fa fa-play-circle"></i></div>Edu Videos</a>	
        							</div>
        							<div class="col-lg-6 col-md-6 col-sm-6">
        								<a href="<?= site_url(); ?>/category/literature" class="vidya_btn vidya_btn_square" style="background-color: #91009b;"><div class="vidya_btn_ico"><i class="fa fa-pencil-square-o"></i></div>Literature</a>
        							</div>
        						</div>
        						<a href="<?= site_url(); ?>/category/vidyaprabhaatham-english" class="vidya_btn" style="background-color: #3b83d7;"><div class="vidya_btn_ico"><i class="fa fa-book"></i></div>Vidyaprabhaatham English</a>
        					</div>
        					<div class="col-lg-2 col-md-2 col-sm-2">
        						<a class="vidya_btn vidya_btn_square" style="background-color: #ff6600;" href="<?= site_url(); ?>/photos"><div class="vidya_btn_ico"><i class="fa fa-camera"></i></div>Photos</a>
        						<a class="vidya_btn vidya_btn_square" style="background-color: #206701;" href="<?= site_url(); ?>/category/inventions"><div class="vidya_btn_ico"><i class="fa fa-lightbulb-o"></i></div>Inventions</a>
        						<a class="vidya_help_lnk" href="<?= site_url(); ?>/category/helpful-links">Helpful Links</a>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
    
<?php get_footer(); ?>