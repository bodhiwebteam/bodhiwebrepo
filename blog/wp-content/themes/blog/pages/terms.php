<div class="about_wrap">
    <div class="container">
        <div class="cat_page_hd">
            <h3>Terms Of Services</h3>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="about_wrap">
                    <p>Welcome to Suprabhaatham</p>
                    <p>Welcome to the website of the Kozhikode Iqrau Publications ("Suprabhaatham"). The Kozhikode Iqrau Publications is a Society incorporated under the laws of the India and is registered with the Registrar of Newspapers for India. With its principal main office at, Francis Road, Calicut -3, Kerala, India.</p>
                    <p>Please read and review these Terms of Use carefully before you access or use our site. By accessing the site or using it you acknowledge that you have read, understood and agreed to the terms of use. If you do not agree to the terms of use, you must stop using this website immediately and you may not access the website or use it.</p>
                    <p>We recommend that you print, store or save a copy of these terms of use for future reference.</p>
                    <p>“Suprabhaatham online” is an online news media and our services are diverse and additional terms or policies relevant a service may apply. Those additional terms available with relevant services become part of this TOS if you access those services. Iqrau Publications reserves the right to update the TOS from time to time, in any way, without notice, effective upon the posting of the modified TOS. Your continued use of our service constitutes your acceptance of our current TOS. If you do not agree with any of these terms, you are prohibited from using or accessing this site.</p>
                    <p>The most current version of the TOS can be reviewed by clicking on the "Terms of Service" link available at the bottom of our pages. We hope you find it expedient.</p>
                    <h4>Privacy</h4>
                    <p>Your privacy is important to us, and we design our products with maximum privacy protection to our users. Our privacy policy is written to make disclosure about how you can use our services and how we collect and can use your data. Please read and understand our Privacy Policy by clicking on the “Privacy policy” hypertext link located at the bottom of our Web pages.</p>
                    <h4>Use License</h4>
                    <p>You agree not use services that are owned by us or interoperated by us in a manner that violates our TOS. You agree to take responsibility of your use of our services and will be the sole proprietor of any risks and should hold Iqrau Publications free from such risks or liabilities. By using our services you must abide by all civil and moral values, conduct in lawful manner, causing no harm nor infringe our property or any third party property. We have the right to review, remove or refuse to display contents that violates our policies or values and or law. All rights, title and interest (copyrights, trademarks, and intellectual property rights and contents) provided by us through our services are owned by us or its respective owners, and you may not use the content from our services unless you obtain permission from its owners or otherwise permitted by law. You have no right to use any logos or branding used in our services. You agree not to use or provide software (except for general purpose web browsers and email clients, or software expressly licensed by us) or services that interact or interoperate with Iqrau Publications, e.g. for downloading, uploading, posting, flagging, emailing, search, or mobile use. Robots, spiders, scripts, scrapers, crawlers, etc. are prohibited, as are misleading, unsolicited, unlawful, and/or spam postings/email. You agree not to collect users' personal and/or contact information. You acknowledge that Iqrau Publications content , services and programs ( which may include audio , text, photographs, graphics or other material contained in any advertising contacts sponsor , or messages , whether by its owners , advertisers, affiliates or partners ) is protected by copyrights , trademarks, signs service , patents and or other proprietary rights and laws , and therefore , only allows you to use the content , service or software , authorized from Iqrau Publications , and its affiliates , advertisers and partners. This license shall automatically terminate if you violate any of these terms and may be terminated by Iqrau Publications at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>
                    <h4>Revisions and Errata</h4>
                    <p>Content, information, programs and products, benefits and services published on this website may include inaccuracies or typographical errors. There will be changes to the contents of the site. Iqrau Publications exercise its right to modify or discontinue any of / or all of the content, information, software, products, features and services published on this website.</p>
                    <p>Iqrau Publications and or associated entities does not claim any form of representation of the appropriateness of the content, information , software, products , benefits and services contained in this site for any purpose . The contents, information, software, products, benefits and services is served "as is" without warranty of any kind. You are advised to refer to individual entities associated with them and their document of DISCLAIMER and WARRANTIES or conditions with respect to such content, information, software, products, benefits and services, including all implied warranties and conditions of MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, title and non-infringement, and availability.</p>
                    <p>In no case shall Iqrau Publications and or associated entities should be responsible for any direct or indirect or punitive damages or incidental, special or consequential arising out of or in any way connected with the use of this site or with the delay or inability to use this site , or for any contents , information, software , products, features and services obtained through this site , or otherwise out of the use of this site , whether based on contract or damage, strict liability or otherwise, even if it has been reported to Iqrau Publications or any of the entities associated with it, about the possibility of damage.</p>
                    <h4>User License</h4>
                    <p>You agree to indemnify and hold Iqrau Publications and its associated entities from the vulnerability of any claim. You agree to compensate Iqrau Publications for; damage or expense, including reasonable attorneys 'fees' on the use of Iqrau Publications Site or violation of this Agreement.</p>
                    <p>As a condition of your use of this Site, you warrant to Iqrau Publications you will not use this Site for any purpose that is unlawful or prohibited by these or any of the other terms, conditions and notices contained in this site.</p>
                    <h4>Links to third party websites</h4>
                    <p>This site may contain links to sites owned by third party entities, all such links are offered for your reference and convenience only. No Iqrau Publications or entities associated with Iqrau Publications have control over these sites, and is not responsible for their contents. The inclusion of Iqrau Publications and its links in their sites does not imply any endorsement of the material in such websites or any association with their operators.</p>
                    <p>The use of message boards and chat rooms and other communication forums</p>
                    <p>If this site contains message or bulletin boards, chat rooms, or other means of communication or message (collectively, "Forums "), you agree to use the Forums only to send and receive messages and material that are proper and related to the forum - for example to note few –You agree that when using the forum, you may not do any of the following:</p>
                    <ul>
                        <li>Defame or abuse others,harass,stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity).</li>
                        <li>Publish or distribute any defamatory, obscene or longer or inappropriate material or illegal or information.</li>
                        <li>Upload files that contain software or other material protected by intellectual property laws (or by rights of privacy of publicity) unless you own or control the rights thereto or have received the necessary approvals.</li>
                        <li>Upload files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another computer, conduct surveys or re- send messages, contests, or chain.</li>
                        <li>Download any file posted by another user of the forum whom you know, or reasonably should know, cannot be legally distributed in such manner.</li>
                        <li>All forums are public and arenot private communications.</li>
                    </ul>
                        <p>Chats, postings, conferences, and other communications by other users are not sanctioned by Iqrau Publications, and should not be considered as such contacts have been reviewed, screened, or approved by the Iqrau Publications.</p>
                        <p>Iqrau Publications and associated service providers reserves the right for any reason to remove the contents of the Forums received from users without notice , including without limitation appointments on the message board.</p>
                        <h4>Moderation in the exchange of information</h4>
                        <p>In addition to the provision of services, English will be used as a platform for all its members to exchange information that will enhance the marketing objectives. It must be clearly understood that the English will not only provide a connection between and or through its service members in the framework and not the direct contact details of those members. Iqrau Publications is not the agent of any member, nor participate in the exchange or its results. Iqrau Publications will be disinterested in the case of disputes between companies or between individuals, although it may make it easier to reach an amicable resolution of disputes of this kind.</p>
                        <h4>Directory listings and services</h4>
                        <p>This site may contain sites and services that enable you to take advantage of them and or to obtain information on the location of users and or services. And you can use to access and use such sites, information, features and services at your own risk. Iqrau Publications does not represent or endorse the accuracy or reliability of any of the contents, information or services are available and listed therein, or that have been obtained using these directories. You acknowledge that any reliance upon any of the information, features, content or services that are available and included in, or that have been obtained using these directories should be at your sole risk.</p>
                        <p>Iqrau Publications provides links and other contact details of publishing entities for your reference and convenience only,Iqrau Publications does not endorse or recommend the services of any of the entities listed in this directory or elsewhere on this site. Any entity determined to communicate or deal with whether listed in this directory or anywhere else on this website solely responsible for its services to you, and you agree that Iqrau Publications will not be responsible for any damages or costs of any type arising out of or in any way connected with your use of these services.</p>
                        <h4>Limitations</h4>
                        <p>In no event shall Iqrau Publications or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on IqrauPublications's collective system, even if Iqrau Publications or a Iqrau Publications authorized representative has been notified orally or in writing of the possibility of such damage.</p>
                        <p>Iqrau Publications disclaims any and all warranties implicit and explicit, including - but not limited to - the implied warranties of merchantability or fitness for a particular purpose and non-infringement. Will not be liable or responsible for those guarantees, warranties and representations, if any, offered by our advertisers in the site, partners and manufacturers of goods or service providers. It is not permissible to act on any advice or information, whether oral or written, obtained from a member of, or through any other source.</p>
                        <p>We respect Copyright Laws; if you find copyright infringement on your content by us you may contact us at web@suprabhaatham.com.</p>
                        <h4>Notices</h4>
                        <p>All notices will be made available on Suprabhaatham sites with its publication date, Iqrau Publications will not take any responsibility to make it <available class=""></available></p>
                        <h4>DISCLAIMER</h4>
                        <p class="text-uppercase">DISCLAIMER OF LIABILITY; YOU ACKNOWLEDGE EXPLICITLY THAT THE USE OF THE SITE AT YOUR SOLE RISK.</p>
                        <p class="text-uppercase">MANY JURISDICTIONS HAVE LAWS PROTECTING CONSUMERS AND OTHER CONTRACT PARTIES, LIMITING THEIR ABILITY TO WAIVE CERTAIN RIGHTS AND RESPONSIBILITIES. WE RESPECT SUCH LAWS; NOTHING HEREIN SHALL WAIVE RIGHTS OR RESPONSIBILITIES THAT CANNOT BE WAIVED.</p>
                        <p class="text-uppercase">USE OF THIS SITE IS UNAUTHORIZED IN ANY JURISDICTION THAT DOES NOT GIVE EFFECT TO ALL PROVISIONS OF THESE TERMS AND CONDITIONS, INCLUDING, BUT NOT LIMITED TO THIS PARAGRAPH.</p>
                        <p class="text-uppercase">THE AGREEMENT SHALL BE GOVERNED BY THE LAWS OF INDIA. THE COURTS OF LAW AT CALICUT SHALL HAVE EXCLUSIVE JURISDICTION OVER ANY DISPUTES ARISING UNDER THIS AGREEMENT.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="other_news_block">
                    <h3 style="margin-top: 0px;">Latest News</h3>
                    <div class="other_news_inner">
                        <ul>
                            <?php
                            $latestArgs = array(
                                'category' => 7,
                                'numberposts' => 5,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_status' => 'publish'
                            );
                            $latestPosts = get_posts($latestArgs);
                            foreach($latestPosts as $latestPost)
                            {
                                ?>
                                <li><a href="<?= get_permalink($latestPost -> ID) ?>"><?= $latestPost -> post_title; ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="social_face">
                        <h3 class="soc_face_head">Social Face</h3>
                        <div class="social_face_tabs">
                            <!--<span class="active" data-target="#fb_soc"><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt="" /></span>
                            <span data-target="#tw_soc"><img src="<?php bloginfo('template_url'); ?>/images/tw.png" alt="" /></span>-->
                            <div class="social_face_tabs" style="display: block;">
                                <button class="btn btn-primary active" data-target="#fb_soc">Facebook</button>
                                <button class="btn btn-primary" data-target="#tw_soc">Twitter</button>
                            </div>
                            <div class="bd_clear"></div>
                        </div>
                        <div class="social_face_inner active" id="fb_soc">
                            <div class="fb-page" data-href="https://www.facebook.com/Suprabhaatham/" data-tabs="timeline" data-height="490" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Suprabhaatham/"><a href="https://www.facebook.com/Suprabhaatham/">Suprabhaatham Daily</a></blockquote></div></div>
                        </div>
                        <div class="social_face_inner" id="tw_soc">
                            <a class="twitter-timeline"  href="https://twitter.com/Suprabhaatham" data-widget-id="674570918199341056">Tweets by @Suprabhaatham</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>