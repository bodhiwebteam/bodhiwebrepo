<div class="about_wrap">
    <div class="container">
        <div class="cat_page_hd">
            <h3>About Us</h3>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="about_wrap">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td colspan="2">
                                <strong>Corporate Office</strong><br />
                                Francis Road<br />
                                Kozhikode - 673 003<br />
                                Ph: 0495 2306100, 2705500<br />
                                Email: <a href="mailto:">info@suprabhaatham.com</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Trivandrum</strong><br />
                                Samastha Jubilee Soudham<br />
                                MeleThampanoor<br />
                                Trivandrum - 695001<br />
                                Ph: 0471 - 2333303<br />
                                mob: 8589984451<br />
                                Email: <a href="mailto:">tvm@spnews.co.in</a>
                            </td>
                            <td>
                                <strong>Kollam</strong><br />
                                Neslin Towers<br />
                                Opp: Petrol Pump<br />
                                Beach Road<br />
                                Kollam – 691 001<br />
                                Ph: 0474 - 2760550<br />
                                mob: 8589984452<br />
                                Email: <a href="mailto:">kol@spnews.co.in</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Pathanamthitta</strong><br />
                                N.K.R Building<br />
                                Pathanamthitta Town<br />
                                Pathanamthitta<br />
                                pin: 689645<br />
                                ph: 0468 2227622<br />
                                mob: 8589984493<br />
                                Email: <a href="mailto:">ptta@spnews.co.in</a>
                            </td>
                            <td>
                                <strong>Kottayam</strong><br />
                                Room No: S10<br />
                                Excel Towers<br />
                                M.G Road<br />
                                Kottayam - 686 001<br />
                                Ph: 0481-2564359<br />
                                mob: 8589984463<br />
                                Email: <a href="mailto:">ktym@spnews.co.in</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Alappuzha</strong><br />
                                AK Cotters<br />
                                Alappuzha<br />
                                Near kallupalam<br />
                                Pin: 688 011<br />
                                Ph: 0477 2252063<br />
                                Mob: 8589984495<br />
                                Email: <a href="mailto:">alp@spnews.co.in</a>
                            </td>
                            <td>
                                <strong>Ernakulam</strong><br />
                                Devikripa Building<br />
                                Iyyattil Junction<br />
                                Chittoor Road<br />
                                Ernakulam.682 011<br />
                                Ph: 0484- 2355123, 3951234<br />
                                mob: 8589984471<br />
                                Email: <a href="mailto:">ekm@spnews.co.in</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Idukki</strong><br />
                                Ilahiya Complex<br />
                                IInd Floor<br />
                                Moovattupuzha Road<br />
                                Thodupuzha - 685 584<br />
                                Ph: 0486-2255194<br />
                                mob: 8589984462<br />
                                Email: <a href="mailto:">idk@spnews.co.in</a>
                            </td>
                            <td>
                                <strong>Trissur</strong><br />
                                Third Floor<br />
                                Rohini Plaza<br />
                                Kokkala Masjid Road<br />
                                Trissur – 680 021<br />
                                PH: 0487 2446606<br />
                                mob: 8589984472<br />
                                Email: <a href="mailto:">tcr@spnews.co.in</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Malappuram</strong><br />
                                Sunni Mahal<br />
                                Kizhakkethala<br />
                                Malappuram-676 519<br />
                                Ph: 0483 2733312<br />
                                mobi : 8589984481<br />
                                Email: <a href="mailto:">mlp@spnews.co.in</a>
                            </td>
                            <td>
                                <strong>Kozhikode</strong><br />
                                Samastha building<br />
                                Francis Road<br />
                                Kozhikode 673 003<br />
                                Ph: 0495 2705515<br />
                                mob: 8589984482<br />
                                Email: <a href="mailto:">clt@spnews.co.in</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Wayanad</strong><br />
                                Suprabhaatham<br />
                                A.P.S Building<br />
                                Press Club Road<br />
                                Kalpatta 673121<br />
                                Ph: 04936-202642<br />
                                mob: 8589984483<br />
                                Email: <a href="mailto:">wyd@spnews.co.in</a>
                            </td>
                            <td>
                                <strong>Kannur</strong><br />
                                Noble Arcade<br />
                                Opp. Sub Registrar Office<br />
                                Near Training School<br />
                                Kannur- 2<br />
                                Ph: 0497 2702785<br />
                                Mob: 8589984491<br />
                                Email: <a href="mailto:">knr@spnews.co.in</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Kasargod</strong><br />
                                Taj Hotel<br />
                                Opp. New Busstand<br />
                                Kasargod.671 123<br />
                                Ph: 0499 4224492<br />
                                mob: 8589984492<br />
                                Email: <a href="mailto:">kzd@spnews.co.in</a>
                            </td>
                            <td>
                                <strong>Palakkad</strong><br />
                                Sreedevi Residency<br />
                                Near Central Excise Office<br />
                                Mettupalayam Street Road<br />
                                Sulthanpett - 678001<br />
                                Ph: 0491 2545880<br />
                                Mob: 8589984473<br />
                                <a href="mailto:">pkd@spnews.co.in</a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Managing Editor : <a href="mailto:me@spnews.co.in">me@spnews.co.in</a><br />
                                CEO: <a href="mailto:ceo@spnews.co.in">ceo@spnews.co.in</a><br />
                                Depty CEO: <a href="mailto:dceo@spnews.co.in">dceo@spnews.co.in</a><br />
                                Editorial: <a href="mailto:editorial@spnews.co.in">editorial@spnews.co.in</a><br />
                                Online : <a href="mailto:web@spnews.co.in">web@spnews.co.in</a><br />
                                HR Manager: <a href="mailto:hrspnews@gmail.com">hrspnews@gmail.com</a><br />
                                Advertisment: <a href="mailto:advt@spnews.co.in">advt@spnews.co.in</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="other_news_block">
                    <h3 style="margin-top: 0px;">Latest News</h3>
                    <div class="other_news_inner">
                        <ul>
                            <?php
                            $latestArgs = array(
                                'category' => 7,
                                'numberposts' => 5,
                                'orderby' => 'post_date',
                                'order' => 'DESC',
                                'post_status' => 'publish'
                            );
                            $latestPosts = get_posts($latestArgs);
                            foreach($latestPosts as $latestPost)
                            {
                                ?>
                                <li><a href="<?= get_permalink($latestPost -> ID) ?>"><?= $latestPost -> post_title; ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="social_face">
                        <h3 class="soc_face_head">Social Face</h3>
                        <div class="social_face_tabs">
                            <!--<span class="active" data-target="#fb_soc"><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt="" /></span>
                            <span data-target="#tw_soc"><img src="<?php bloginfo('template_url'); ?>/images/tw.png" alt="" /></span>-->
                            <div class="social_face_tabs" style="display: block;">
                                <button class="btn btn-primary active" data-target="#fb_soc">Facebook</button>
                                <button class="btn btn-primary" data-target="#tw_soc">Twitter</button>
                            </div>
                            <div class="bd_clear"></div>
                        </div>
                        <div class="social_face_inner active" id="fb_soc">
                            <div class="fb-page" data-href="https://www.facebook.com/Suprabhaatham/" data-tabs="timeline" data-height="490" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Suprabhaatham/"><a href="https://www.facebook.com/Suprabhaatham/">Suprabhaatham Daily</a></blockquote></div></div>
                        </div>
                        <div class="social_face_inner" id="tw_soc">
                            <a class="twitter-timeline"  href="https://twitter.com/Suprabhaatham" data-widget-id="674570918199341056">Tweets by @Suprabhaatham</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>