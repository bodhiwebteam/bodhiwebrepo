<?php
	get_header();
	$archCat = $_REQUEST['cat'];
	//echo $archCat;
	$archDate;
	if(isset($_REQUEST['arcDate']))
	{
		$archDate = $_REQUEST['arcDate'];
	}
	else
	{
		$archDate = date('Y-m-d');
	}
	$archDateAr = explode('-', $archDate);
if($archCat == 222){
?>
<div class="inner_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="cat_page_hd">
					<h3>Archives of News from print</h3>
				</div>
			</div>
		</div>
		<div class="row">			
			<div class="col-lg-8 col-md-8 col-sm-8 arch_news_wrap">
				<?php
					$archArgs = array(
						'showposts' => -1,
						'cat' => '21, 17, 19, 20, 18, 23, 22',
						'orderby' => 'date',
						'order' => 'DESC',
						'date_query' => array(
							'year' => $archDateAr[0],
							'month' => $archDateAr[1],
							'day' => $archDateAr[2]
						),
						'post_status'   => 'publish'
					);
					$archQuery = new WP_Query($archArgs);
					if ( $archQuery->have_posts() ) {						
						while ( $archQuery->have_posts() ) {
							$archQuery->the_post();
							$image_url = is_null(get_feat_test_image_url(get_the_ID()))?get_template_directory_uri().'/images/default.jpg':get_feat_test_image_url(get_the_ID());
							?>
							<a class="arch_news" href="<?= get_the_permalink(); ?>">
								<div class="arch_news_img">
									<img src="<?= $image_url ?>" alt="" />
								</div>
								<div class="arch_news_content">
									<h4><?= limit_words(get_the_title(), 8); ?></h4>
									<div class="arch_content">
										<?= limit_words(get_the_content(), 15); ?>
									</div>
								</div>
								<div class="bd_clear"></div>
							</a>
							<?php
						}						
					} else {
						echo 'No news found on '.$archDateAr[2].'-'.$archDateAr[1].'-'.$archDateAr[0].'. Please select another date';
					}
				?>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="arch_form">
					<h4>Select a date</h4>
					<form method="get">
						<input id="datepicker" autocomplete="off" type="text" name="date" value="<?= $archDateAr[2].'-'.$archDateAr[1].'-'.$archDateAr[0] ?>" />
						<input id="wp_date" name="arcDate" type="hidden"/>
						<input name="cat" value="<?= $archCat ?>" type="hidden"/>
						<input type="submit" value="Get News"/>
					</form>
				</div>
				<div class="other_news_block">
					<h3 style="margin-top: 0px;">Latest News</h3>
					<div class="other_news_inner">
						<ul>
							<?php
							$latestArgs = array(
								'category' => 7,
								'numberposts' => 5,
								'orderby' => 'post_date',
								'order' => 'DESC',
								'post_status' => 'publish'
							);
							$latestPosts = get_posts($latestArgs);
							foreach($latestPosts as $latestPost)
							{
							?>
							<li><a href="<?= get_permalink($latestPost -> ID) ?>"><?= $latestPost -> post_title; ?></a></li>
							<?php
							}
							?>
						</ul>
					</div>
				</div>	
				<?= adCatPage(); ?>
			</div>
		</div>	
	</div>
</div>
<?php
}
elseif($archCat == 35){
?>
<div class="inner_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="cat_page_hd">
					<h3>Archives of ഞായർ പ്രഭാതം</h3>
				</div>
			</div>
		</div>
		<div class="row">			
			<div class="col-lg-8 col-md-8 col-sm-8 arch_news_wrap">
				<?php				
					$archDateToDateTime = new DateTime($archDate);
					$week = $archDateToDateTime->format('W');
					$archArgs = array(
						'showposts' => -1,
						'cat' => '169, 172, 165, 166, 171, 170, 164, 168, 167, 174',
						'orderby' => 'date',
						'order' => 'DESC',
						'date_query' => array(
							'year' => $archDateAr[0],
							'week' => $week
						),
						'post_status'   => 'publish'
					);
					$archQuery = new WP_Query($archArgs);
					if ( $archQuery->have_posts() ) {						
						while ( $archQuery->have_posts() ) {
							$archQuery->the_post();
							$image_url = is_null(get_feat_test_image_url(get_the_ID()))?get_template_directory_uri().'/images/default.jpg':get_feat_test_image_url(get_the_ID());
							?>
							<a class="arch_news" href="<?= get_the_permalink(); ?>">
								<div class="arch_news_img">
									<img src="<?= $image_url ?>" alt="" />
								</div>
								<div class="arch_news_content">
									<h4><?= limit_words(get_the_title(), 8); ?></h4>
									<div class="arch_content">
										<?= limit_words(get_the_content(), 15); ?>
									</div>
								</div>
								<div class="bd_clear"></div>
							</a>
							<?php
						}						
					} else {
						echo 'No news found on the selected week. Please select another week.';
					}
				?>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="arch_form">
					<h4>Select a week</h4>
					<form method="get">
						<input id="datepicker" autocomplete="off" type="text" name="date" value="<?= $archDateAr[2].'-'.$archDateAr[1].'-'.$archDateAr[0] ?>" />
						<input id="wp_date" name="arcDate" type="hidden"/>
						<input name="cat" value="<?= $archCat ?>" type="hidden"/>
						<input type="submit" value="Get News"/>
					</form>
				</div>
				<div class="other_news_block">
					<h3 style="margin-top: 0px;">Latest News</h3>
					<div class="other_news_inner">
						<ul>
							<?php
							$latestArgs = array(
								'category' => 7,
								'numberposts' => 5,
								'orderby' => 'post_date',
								'order' => 'DESC',
								'post_status' => 'publish'
							);
							$latestPosts = get_posts($latestArgs);
							foreach($latestPosts as $latestPost)
							{
							?>
							<li><a href="<?= get_permalink($latestPost -> ID) ?>"><?= $latestPost -> post_title; ?></a></li>
							<?php
							}
							?>
						</ul>
					</div>
				</div>	
				<?= adCatPage(); ?>
			</div>
		</div>	
	</div>
</div>
<?php
} elseif($archCat == 9) {
?>
	<div class="inner_wrap">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cat_page_hd">
						<h3>Archives of Don't Miss It</h3>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 arch_news_wrap">
					<?php
					$archArgs = array(
						'showposts' => -1,
						'cat' => '9',
						'orderby' => 'date',
						'order' => 'DESC',
						'date_query' => array(
							'year' => $archDateAr[0],
							'month' => $archDateAr[1],
							'day' => $archDateAr[2]
						),
						'post_status'   => 'publish'
					);
					$archQuery = new WP_Query($archArgs);
					if ( $archQuery->have_posts() ) {
						while ( $archQuery->have_posts() ) {
							$archQuery->the_post();
							$image_url = is_null(get_feat_test_image_url(get_the_ID()))?get_template_directory_uri().'/images/default.jpg':get_feat_test_image_url(get_the_ID());
							?>
							<a class="arch_news" href="<?= get_the_permalink(); ?>">
								<div class="arch_news_img">
									<img src="<?= $image_url ?>" alt="" />
								</div>
								<div class="arch_news_content">
									<h4><?= limit_words(get_the_title(), 8); ?></h4>
									<div class="arch_content">
										<?= limit_words(get_the_content(), 15); ?>
									</div>
								</div>
								<div class="bd_clear"></div>
							</a>
							<?php
						}
					} else {
						echo 'No news found on '.$archDateAr[2].'-'.$archDateAr[1].'-'.$archDateAr[0].'. Please select another date';
					}
					?>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<div class="arch_form">
						<h4>Select a date</h4>
						<form method="get">
							<input id="datepicker" autocomplete="off" type="text" name="date" value="<?= $archDateAr[2].'-'.$archDateAr[1].'-'.$archDateAr[0] ?>" />
							<input id="wp_date" name="arcDate" type="hidden"/>
							<input name="cat" value="<?= $archCat ?>" type="hidden"/>
							<input type="submit" value="Get News"/>
						</form>
					</div>
					<div class="other_news_block">
						<h3 style="margin-top: 0px;">Latest News</h3>
						<div class="other_news_inner">
							<ul>
								<?php
								$latestArgs = array(
									'category' => 7,
									'numberposts' => 5,
									'orderby' => 'post_date',
									'order' => 'DESC',
									'post_status' => 'publish'
								);
								$latestPosts = get_posts($latestArgs);
								foreach($latestPosts as $latestPost)
								{
									?>
									<li><a href="<?= get_permalink($latestPost -> ID) ?>"><?= $latestPost -> post_title; ?></a></li>
									<?php
								}
								?>
							</ul>
						</div>
					</div>
					<?= adCatPage(); ?>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>
        
<?php get_footer(); ?>