<?php
get_header();
?>
<div class="archieve_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="cat_page_hd">
					<h3>Info</h3>
				</div>
			</div>
		</div>		
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8">
				<div class="panel-group info_panels">
					<div class="panel panel-primary">
						<div class="panel-heading">Career</div>
						<div class="panel-body">
							<ul>
							<?php
								$careerArgs = array(
									'category' => 204,
									'numberposts' => 5,
									'orderby' => 'post_date',
									'order' => 'DESC',
									'post_status' => 'publish'
								);
								$careerPosts = get_posts($careerArgs);
								if(count($careerPosts) < 1){
									echo '<li>There is no data.</li>';
								} else {
									foreach($careerPosts as $careerPost){
										$image_url = get_feat_test_image_url($careerPost->ID);
										?>
										<li>
											<a href="<?= get_permalink($careerPost->ID); ?>">
												<div class="career_img"><img src="<?= $image_url; ?>" alt="" /></div>
												<h3 class="career_news"><?= $careerPost->post_title; ?></h3>
												<div class="bd_clear"></div>
											</a>
										</li>
										<?php
									}
								}
							?>
							</ul>
						</div>
						<div class="panel-footer">
							<a class="btn btn-primary" href="<?= site_url(); ?>/category/career">See More</a>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">Govt. Info</div>
						<div class="panel-body">
							<ul>
							<?php
								$careerArgs = array(
									'category' => 205,
									'numberposts' => 5,
									'orderby' => 'post_date',
									'order' => 'DESC',
									'post_status' => 'publish'
								);
								$careerPosts = get_posts($careerArgs);
								if(count($careerPosts) < 1){
									echo '<li>There is no data.</li>';
								} else {
									foreach($careerPosts as $careerPost){
										$image_url = get_feat_test_image_url($careerPost->ID);
										?>
										<li>
											<a href="<?= get_permalink($careerPost->ID); ?>">
												<div class="career_img"><img src="<?= $image_url; ?>" alt="" /></div>
												<h3 class="career_news"><?= $careerPost->post_title; ?></h3>
												<div class="bd_clear"></div>
											</a>
										</li>
										<?php
									}
								}
							?>
							</ul>
						</div>
						<div class="panel-footer">
							<a class="btn btn-primary" href="<?= site_url(); ?>/category/govt-info">See More</a>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">Universities</div>
						<div class="panel-body">
							<ul>
							<?php
								$careerArgs = array(
									'category' => 206,
									'numberposts' => 5,
									'orderby' => 'post_date',
									'order' => 'DESC',
									'post_status' => 'publish'
								);
								$careerPosts = get_posts($careerArgs);
								if(count($careerPosts) < 1){
									echo '<li>There is no data.</li>';
								} else {
									foreach($careerPosts as $careerPost){
										$image_url = get_feat_test_image_url($careerPost->ID);
										?>
										<li>
											<a href="<?= get_permalink($careerPost->ID); ?>">
												<div class="career_img"><img src="<?= $image_url; ?>" alt="" /></div>
												<h3 class="career_news"><?= $careerPost->post_title; ?></h3>
												<div class="bd_clear"></div>
											</a>
										</li>
										<?php
									}
								}
							?>
							</ul>
						</div>
						<div class="panel-footer">
							<a class="btn btn-primary" href="<?= site_url(); ?>/category/universities">See More</a>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">Others</div>
						<div class="panel-body">
							<ul>
							<?php
								$careerArgs = array(
									'category' => 207,
									'numberposts' => 5,
									'orderby' => 'post_date',
									'order' => 'DESC',
									'post_status' => 'publish'
								);
								$careerPosts = get_posts($careerArgs);
								if(count($careerPosts) < 1){
									echo '<li>There is no data.</li>';
								} else {
									foreach($careerPosts as $careerPost){
										$image_url = get_feat_test_image_url($careerPost->ID);
										?>
										<li>
											<a href="<?= get_permalink($careerPost->ID); ?>">
												<div class="career_img"><img src="<?= $image_url; ?>" alt="" /></div>
												<h3 class="career_news"><?= $careerPost->post_title; ?></h3>
												<div class="bd_clear"></div>
											</a>
										</li>
										<?php
									}
								}
							?>
							</ul>
						</div>
						<div class="panel-footer">
							<a class="btn btn-primary" href="<?= site_url(); ?>/category/others">See More</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
						
				<div class="other_news_block">
					<h3 style="margin-top: 0px;">Trending News</h3>
					<div class="other_news_inner">
						<ul>
							<?= trendingNewsArchive(); ?>
						</ul>
					</div>
				</div>	
				<?= adCatPage(); ?>
	            <div class="social_face">
                    <h3 class="soc_face_head">Social Face</h3>
                    <div class="social_face_tabs">
                        <div class="social_face_tabs" style="display: block;">
                            <button class="btn btn-primary active" data-target="#fb_soc">Facebook</button>
                            <button class="btn btn-primary" data-target="#tw_soc">Twitter</button>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                    <div class="social_face_inner active" id="fb_soc">
                        <div class="fb-page" data-href="https://www.facebook.com/Suprabhaatham/" data-tabs="timeline" data-height="490" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Suprabhaatham/"><a href="https://www.facebook.com/Suprabhaatham/">Suprabhaatham Daily</a></blockquote></div></div>
                    </div>
                    <div class="social_face_inner" id="tw_soc">
                    	<a class="twitter-timeline"  href="https://twitter.com/Suprabhaatham" data-widget-id="674570918199341056">Tweets by @Suprabhaatham</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>


                    </div>
                </div> 
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
?>