 <?php get_header(); ?>
 <!-- Content -->        
    <div class="art_page_wrap">
        <div class="container" id="art_pop">
        	<div class="row">
        		<?php
        			$artArgs = array(
        				'category' => 182,
						'numberposts' => -1,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_status' => 'publish'
        			);
        			$artPosts = get_posts($artArgs);
        			$i = 0;
        			foreach($artPosts as $artPost){
        				$image_thumb = get_feat_test_image_url($artPost->ID);
        				$image_url = get_post_featured_image_url($artPost->ID);
					?>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<a class="art_box" href="<?= $image_url; ?>" data-sub-html="<h3><?= get_post_meta($artPost->ID, 'author', TRUE); ?></h3>">
							<div class="art_box_img">
								<img src="<?= $image_thumb; ?>" alt=""/>
								<div class="art_overlay"></div>
							</div>							
							<h4><?= $artPost->post_title; ?></h4>							
						</a>
					</div>
					<?php
					
						$i += 1;
						if($i == 3){
							?>
							</div><div class="row">
							<?php
							$i = 0;
						}
					}
        		?>
        	</div>
        </div>
    </div>
    
    
    <script src="<?php bloginfo('template_url'); ?>/light/lightgallery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/light/lg-fullscreen.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/light/lg-thumbnail.min.js"></script>
    <script>
    	jQuery(document).ready(function($){
		  //you can now use $ as your jQuery object.
		  $('#art_pop').lightGallery({
    			download: false,
                thumbnail: false,
                fullScreen: false,
                selector: '.row .col-lg-4 a'
    		});
		});
    </script>
    
<?php get_footer(); ?>