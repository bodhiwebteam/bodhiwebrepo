 <?php get_header(); ?>
 <!-- Content -->        
    <div class="inner_wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="sunday_banner">
                        <?= sundayBanner(); ?>
                    </div>
                </div>
            </div>
            
            <div class="row">
            	<div class="col-lg-8 col-md-8 col-sm-8">
            		<div class="row">
            			<div class="col-lg-6 col-md-6 col-sm-6">
		            		<?= nattupacha(); ?>
		            	</div>
		            	<div class="col-lg-6 col-md-6 col-sm-6">
		            		<?= ulkazhcha(); ?>
		            	</div>
            		</div>
            		<?= sundaySecondary(); ?>
            		<div class="camera_wrap">
            			<div class="camera_cap"></div>
            			<?= cameraKuripp(); ?>
            		</div>	
            	</div>
            	<div class="col-lg-4 col-md-4 col-sm-4">
            		<?= padanam(); ?>
            		<?= pusthakam(); ?>
            		<?= kurunkatha(); ?>
            	</div>
            </div>
        </div>
        <div class="container vaayana_wrap" style="margin-top: 25px;">
        	<div class="row">
        		<div class="col-lg-12">
        			<div class="vaayana_head">
        				<div class="vaayana_hd">
        					<i class="fa fa-book"></i> വായന
        				</div>
        				<a href="<?= site_url(); ?>/category/vaayana">Sea More <i class="fa fa-angle-right"></i></a>
        				<div class="bd_clear"></div>
        			</div>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-lg-12">
        			<div class="vayana_bg">
        				<div class="row">
        					<?= vaayana(); ?>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
    
<?php get_footer(); ?>