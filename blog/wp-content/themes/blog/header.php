<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="fb:pages" content="293809530773903" />
<meta property="fb:use_automatic_ad_placement" content="enable=true ad_density=default">
    <title>Blog</title>
    <?php if (is_single()) {
        $fbPostId = get_the_ID();
        $fbCurPost = get_post($fbPostId);
        $fbPostTitle = $fbCurPost->post_title;
        $fbPostDesc = limit_words(strip_tags($fbCurPost->post_content), 15).'...';
        $fbPostsLnk = get_permalink();
        $fbPostImg = get_post_featured_image_url($fbPostId);
        if (!is_null($fbPostImg)) {
            $fbShareImg = $fbPostImg;
        } else {
            $fbShareImg = bloginfo('template_url').'/images/default.jpg';
        }
        ?>
        <meta property="fb:app_id" content="675664959269324" />
        <meta property="og:url" content="<?= $fbPostsLnk ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?= $fbPostTitle ?>" />
        <meta property="og:description" content="<?= $fbPostDesc ?>" />
        <meta property="og:image" content="<?= $fbShareImg ?>" />
        <?php
    } ?>
    <link rel="icon" href="<?= bloginfo('template_url'); ?>/images/fav_ico.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jQuerySimpleCounter.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
    <?php wp_head(); ?>
    
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58585735-1', 'auto');
  ga('send', 'pageview');

</script>


</head>
<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7&appId=2006095599616010";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
<div class="career_banner_wrap">
    <div class="container">
        <h2>Blog</h2>
        <h4>A space for knowledge</h4>
    </div>
</div>
<div class="header">
    <div class="container">
        <div class="logo">
            <a href="http://www.bodhiinfo.com"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Bodhi"></a>
        </div>
        <div class="nav_wrap">
            <span class="nav_trigger"><i class="fa fa-bars"></i></span>
            <ul class="navigation">
                <li>
                    <a href="http://www.bodhiinfo.com" title="Home">
                        home
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.bodhiinfo.com/about.html" title="about">
                        about
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.bodhiinfo.com/service.html" title="services">
                        services
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.bodhiinfo.com/products.html" title="products">
                        products
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.bodhiinfo.com/portfolio.html" title="portfolio">
                        portfolio
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.bodhiinfo.com/career.html" title="career">
                        career
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.bodhiinfo.com/team.html" title="team">
                        team
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url(); ?>" class="active" title="blog">
                        blog
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.bodhiinfo.com/social-commitment.html" title="social commitment">
                        social commitment
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
                <li>
                    <a href="http://www.bodhiinfo.com/contact.html" title="Reach Us">
                        Reach Us
                        <span class="nav_border nav_border_top"></span>
                        <span class="nav_border nav_border_right"></span>
                        <span class="nav_border nav_border_bottom"></span>
                        <span class="nav_border nav_border_left"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>