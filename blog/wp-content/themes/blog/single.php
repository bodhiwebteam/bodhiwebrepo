<?php
get_header();

$current_post_id = get_the_ID();

$current_post = get_post($current_post_id);

$featured_image = get_post_featured_image_url($current_post_id);

$postUrl = get_permalink();
?>
<div class="blog_wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="main_post_img">
                    <img src="<?= $featured_image; ?>" alt="">
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="main_post_date">
                            <h2><span><b><?= get_the_date( 'd', $hmPost->ID ); ?></b><?= get_the_date( 'S', $hmPost->ID ); ?></span><br /><?= get_the_date( 'M', $hmPost->ID ); ?><br /><?= get_the_date( 'Y', $hmPost->ID ); ?></h2>
                        </div>
                        <div class="main_post_heading">
                            <h2><?= $current_post->post_title; ?></h2>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="soc_auth clearfix">
                            <div class="soc pull-left">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $postUrl; ?>" target="_blank" class="fb"><i class="ion ion-social-facebook"></i></a>
                                <a href="https://twitter.com/home?status=<?= $postUrl ?>" target="_blank" class="tw"><i class="ion ion-social-twitter"></i></a>
                                <a href="https://plus.google.com/share?url=<?= $postUrl ?>" target="_blank" class="gp"><i class="ion ion-social-googleplus"></i></a>
                            </div>
                            <div class="auth pull-right">
                                <i class="ion ion-ios-compose"></i> <?= the_author_meta( 'display_name' , $current_post->post_author); ?>
                            </div>
                        </div>
                        <div class="main_post_content">
                            <?= apply_filters('the_content', $current_post->post_content); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="recent_posts">
                    <h3>Recent Posts</h3>
                    <?php
                    $recentPostsArgs = array('posts_per_page' => 5, 'post_type' => 'post', 'orderby' => 'date', 'order' => 'DESC', 'post_status' => 'publish', 'post__not_in' => array($current_post_id));
                    $recentPosts = query_posts($recentPostsArgs);
                    foreach ($recentPosts as $recPost) {
                        $image_url = get_feat_thumb_img($recPost->ID);
                        $url = get_permalink($recPost->ID);
                        ?>
                        <div class="recent_posts_box">
                            <div class="recent_post_temp_img">
                                <a href="<?= $url; ?>"><img src="<?= $image_url; ?>" alt=""></a>
                            </div>
                            <div class="recent_post_temp_content">
                                <a href="<?= $url; ?>"><h5><?= $recPost->post_title; ?></h5></a>
                                <p><?= get_the_date( 'dS', $recPost->ID ); ?>  |  <?= get_the_date( 'M', $recPost->ID ); ?>  |  <?= get_the_date( 'Y', $recPost->ID ); ?></p>
                            </div>
                        </div>
                        <hr>
                        <?php
                    }
                    ?>
                </div>
                <div class="fb_feed" style="margin-top: 15px;">
                    <div class="fb-page" data-href="https://www.facebook.com/bodhiinfo/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"><blockquote cite="https://www.facebook.com/bodhiinfo/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bodhiinfo/">Bodhi Info Solutions Pvt. Ltd.</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>