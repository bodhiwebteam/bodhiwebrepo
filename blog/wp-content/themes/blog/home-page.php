<div class="blog_wrap">
    <div class="container">
        <div class="row">
            <h1></h1>
            <div class="col-lg-8 col-md-8 col-sm-8">
                <?php
                if ( get_query_var('paged') ) {
                    $paged = get_query_var('paged');
                } else if ( get_query_var('page') ) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $hmPostArgs = array('posts_per_page' => 3, 'post_type' => 'post', 'paged' => $paged, 'orderby' => 'date', 'order' => 'DESC', 'post_status' => 'publish');
                $homePosts = query_posts($hmPostArgs);
                $i = 0;
                $exclusionArray = array();
                foreach ($homePosts as $hmPost) {
                    $image_url = get_feat_test_image_url($hmPost->ID);
                    $url = get_permalink($hmPost->ID);
                    array_push($exclusionArray, $hmPost->ID);
                    if ($i == 0) {
                        ?>
                        <div class="main_post_img">
                             <a href="<?= $url; ?>" title="<?= $hmPost->post_title; ?>"><img src="<?= $image_url; ?>" alt=""></a>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="clearfix">
                                    <div class="main_post_date">
                                        <h2><span><b><?= get_the_date( 'd', $hmPost->ID ); ?></b><?= get_the_date( 'S', $hmPost->ID ); ?></span><br />
                                            <?= get_the_date( 'M', $hmPost->ID ); ?><br />
                                            <?= get_the_date( 'Y', $hmPost->ID ); ?>
                                        </h2>
                                    </div>
                                    <div class="main_post_heading">
                                         <a href="<?= $url; ?>" title="<?= $hmPost->post_title; ?>"><h2><?= $hmPost->post_title; ?></h2></a>
                                    </div>
                                </div>
                                <div class="main_post_content">
                                    <div style="margin-bottom: 30px; margin-top: 15px;"><?= limit_words(strip_tags($hmPost->post_content), 50); ?>...</div>
                                    <a href="<?= $url; ?>" title="<?= $hmPost->post_title; ?>"><span class="more"><img src="<?php bloginfo('template_url'); ?>/images/more.png" alt=""><b>MORE</b></span></a>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <?php
                    } else {
                    	$image_thumb_url = get_feat_thumb_img($hmPost->ID);
                        ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="old_posts_img">
                                     <a href="<?= $url; ?>" title="<?= $hmPost->post_title ?>"><img src="<?= $image_thumb_url; ?>" alt=""></a>
                                </div>
                                <div class="old_posts_content">
                                    <a href="<?= get_permalink($hmPost->ID) ?>"  title="<?= $hmPost->post_title ?>">
                                        <h3><?= $hmPost->post_title ?></h3>
                                        <span class="blue_line"></span></a>
                                    <h4><?= get_the_date( 'dS', $hmPost->ID ); ?>  |  <?= get_the_date( 'F', $hmPost->ID ); ?>  |  <?= get_the_date( 'Y', $hmPost->ID ); ?></h4>
                                    <div style="margin-bottom:30px">
                                        <?= limit_words(strip_tags($hmPost->post_content), 20); ?>...
                                    </div>
                                    <a href="<?= $url ?>" title="<?= $hmPost->post_title ?>"><span class="more"><img src="<?php bloginfo('template_url'); ?>/images/more.png" alt=""><b>MORE</b></span></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <?php
                    }
                    $i += 1;                    
                }
                ?>
                <div class="pager_wrap">
                    <nav class="pagination clearfix">
                        <?php 
                        pagination_bar();
                        wp_reset_query();
                        ?>
                    </nav>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="recent_posts">
                    <h3>Recent Posts</h3>
                    <?php
                    $recentPostsArgs = array('posts_per_page' => 5, 'post_type' => 'post', 'orderby' => 'date', 'order' => 'DESC', 'post_status' => 'publish', 'post__not_in' => $exclusionArray);
                    $recentPosts = query_posts($recentPostsArgs);
                    foreach ($recentPosts as $recPost) {
                        $image_url = get_feat_thumb_img($recPost->ID);
                        $url = get_permalink($recPost->ID);
                        ?>
                        <div class="recent_posts_box">
                            <div class="recent_post_temp_img">
                                <a href="<?= $url; ?>" title="<?= $recPost->post_title; ?>"><img src="<?= $image_url; ?>" alt=""></a>
                            </div>
                            <div class="recent_post_temp_content">
                                <a href="<?= $url; ?>" title="<?= $recPost->post_title; ?>"><h5><?= $recPost->post_title; ?></h5></a>
                                <p><?= get_the_date( 'dS', $recPost->ID ); ?>  |  <?= get_the_date( 'M', $recPost->ID ); ?>  |  <?= get_the_date( 'Y', $recPost->ID ); ?></p>
                            </div>
                        </div>
                        <hr>
                        <?php
                    }
                    wp_reset_query();
                    ?>
                </div>
                <div class="fb_feed" style="margin-top: 15px;">
                    <div class="fb-page" data-href="https://www.facebook.com/bodhiinfo/" title="facebook" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"><blockquote cite="https://www.facebook.com/bodhiinfo/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bodhiinfo/" title="facebook">Bodhi Info Solutions Pvt. Ltd.</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
</div>