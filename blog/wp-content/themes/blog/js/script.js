/**
 * Created by shafeequemat on 08-04-2016.
 */
$(function () {
    $('.nav_trigger').click(function () {
        $('.navigation').slideToggle();    
    });
    $('.main_post_content img').removeAttr('width').removeAttr('height');
    //Trigger counter on scrolling
    var scrollCheck = true;
    $(window).scroll(function () {
        var windTop = $(window).scrollTop(),
            footerTop = $('footer').position().top;
        if (scrollCheck && Math.abs(footerTop - windTop) < 200) {
            $('#count-test-2').jQuerySimpleCounter({
                start:  0,
                end:    200,
                duration: 4000
            });
            $('#count-test-3').jQuerySimpleCounter({
                start:  0,
                end:    100,
                duration: 5000
            });
            $('#count-test-4').jQuerySimpleCounter({
                start:  0,
                end:    5,
                duration: 1000
            });
            scrollCheck = false;
        }
    });
});
