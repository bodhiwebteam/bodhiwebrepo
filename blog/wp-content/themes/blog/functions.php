<?php

/**
 * Slightly Modified Options Framework
 */
require_once ('admin/index.php');

function getUserIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) //if from shared
    {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //if from a proxy
    {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}

// Word limiter. Don't touch it. Too Risky :P

function limit_words($string, $word_limit)
{
	$words = explode(" ",$string);
	return implode(" ",array_splice($words,0,$word_limit));
}

// Add Menu support. Only Wordpress know what is this

//add_theme_support( 'menus' );
register_nav_menu( 'mainmenu', 'Main Menu' );
register_nav_menu( 'footermenu', 'Footer Menu' );
add_theme_support( 'post-thumbnails' );
function rh_show_menu($data)
{
	if($data['type'] == 'basic')
	{
		$registred_locations = get_registered_nav_menus();
		$items = wp_get_nav_menu_items($registred_locations[$data['slug']]);
		$current_page_id = get_the_id();

		foreach($items as $item)
			{


				$active_css_class = NULL;
				if($current_page_id == $item->object_id)
				{$active_css_class = 'active';}
				$html = $html."<li><a href=".$item->url.">".$item->title."</a></li>";
			}
		echo $html;



	}
	else
	{
	$defaults = array(
						'theme_location'  => $data['slug'],
						'menu'            => '',
						'container'       => false,
						'container_class' => $data['class'],
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul class="'.$data['class'].'">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
	wp_nav_menu( $defaults );
	}
}

// Get URL of first image in a post. It is a particular Kariyila. Don't tuch it.

function catch_that_image($contnent)
{
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $contnent, $matches);
	$first_img = $matches [1] [0];
	// No Image found. Total fail :-D
	if(empty($first_img))
	{
		$first_img = get_template_directory_uri()."/images/default.jpg";
	}
return $first_img;
}

function get_post_featured_image_url($id)
{
	$featured_image = get_the_post_thumbnail($id, 'full');
	$image_url = catch_that_image($featured_image);
	return $image_url;
}
function get_feat_test_image_url($id)
{
	$feat_test_image = get_the_post_thumbnail($id, 'normal-thumb');
	$feat_test_image_url = catch_that_image($feat_test_image);
	return $feat_test_image_url;
}
function get_feat_thumb_img($id)
{
	$feat_thumb_img = get_the_post_thumbnail($id, 'thumbnail');
	$feat_thumb_img_url = catch_that_image($feat_thumb_img);
	return $feat_thumb_img_url;
}

// Remove Images Just text

function remove_image_filter($content_text){
$filtered_text = preg_replace("/<img[^>]+\>/i", "", $content_text);
return $filtered_text;
}
// Widgets
if ( function_exists('register_sidebar') )
register_sidebar(array('name' => 'Weather','before_widget' => '','after_widget' => '','before_title' => '<h1>','after_title' => '</h1>'));

if ( function_exists('register_sidebar') )
register_sidebar(array('name' => 'Home Right','before_widget' => '','after_widget' => '','before_title' => '<h1>','after_title' => '</h1>'));

//Return Page type
function current_page_type($page_ID){
$result = get_post_meta($page_ID, 'pagetype', true);
return $result;
}

//Get Category id with name
function get_category_id($cat_name){
	$term = get_term_by('name', $cat_name, 'category');
	return $term->term_id;
}


//add_image_size( 'test-thumb', 850, 350, TRUE);
add_image_size('normal-thumb', 809, 415, TRUE);

function get_template_option_values($input)
{
	global $data;


	return $data[$input];
}

function get_home_page_text()
{
	$about_id = 2;
	$page_data = get_page($about_id);
	$page_text = $page_data->post_content;
	$page_url = get_permalink($about_id);
	$html = '<h1><span>Welcome to Mafco</span></h1>
    <p>'.$page_text.'</p><a href="'.$page_url.'" class="rh_more_btn">more</a>';
    return $html;
}

function get_product_names_helper($catid)
{
	$post_array = get_posts(  array(
		'numberposts'		=>	1000,
		'offset'			=>	0,
		'category'			=>	$catid,
		'orderby'			=>	'post_date',
		'order'				=>	'DESC',
		'include'			=>	'',
		'exclude'			=>	'',
		'meta_key'			=>	'',
		'meta_value'		=>	'',
		'post_type'			=>	'post',
		'post_mime_type'	=>	'',
		'post_parent'		=>	'',
		'post_status'		=>	'publish' )
	);
	foreach($post_array as $post)
	{
		$url = get_permalink($post->ID);
		$html = $html.'<li><a href="'.$url.'">'.$post->post_title.'</a></li>';
	}
	return '<ul class="rh_prdct_submenu">'.$html.'</ul>';
}
function show_client_logo()
{
	global $wpdb;
	$tablename_images = $wpdb->prefix."ngg_pictures";
	$tablename_gallery = $wpdb->prefix."ngg_gallery";
	$query = "SELECT * FROM $tablename_images WHERE galleryid = 1";
	$query_result = $wpdb->get_results($query, ARRAY_A);
	foreach($query_result as $images)
	{
		$gallery_id = $images['galleryid'];
		$get_directory_query = 	"SELECT path FROM $tablename_gallery WHERE gid = '$gallery_id'";
		$directory_name = $wpdb->get_var($get_directory_query);
		$image_url = get_bloginfo('url')."/".$directory_name."/".$images['filename'];
		$html = $html."<li><img src='".$image_url."' width='195' height='110'></li>";

	}
	return $html;
}
function show_main_banner()
{
	$data_array = get_template_option_values('pingu_slider');
	foreach ($data_array as $slide) {
		# code...
		$html = $html.'<div style=\'background-image: url("'.$slide['url'].'");\'></div>';
	}
	return $html;


}
function show_main_banner_1()
{
	$data_array = get_template_option_values('pingu_slider');
	foreach ($data_array as $slide) {
		# code...
		$html = $html.'<div class="da-slide">'.
					'<h2>'.$slide['title'].'</h2>'.
					'<p>'.$slide['description'].'</p>'.
					'<a href="'.$slide['link_url'].'" class="da-link">shop now</a>'.

					'<div class="da-img"><img src="'.$slide['url'].'"  alt="'.$slide['title'].'" /></div>'.
				'</div>';
	}
	return $html;
}

// Pagination
function pagination_bar() {
	$web_path = site_url().'/wp-content/themes/suprabhatham';
	global $wp_query;

	$total_pages = $wp_query->max_num_pages;

	if ($total_pages > 1){
		$current_page = max(1, get_query_var('paged'));

		echo paginate_links(array(
			'title' => "123",
			'base' => get_pagenum_link(1) . '%_%',
			'format' => '/page/%#%',
			'current' => $current_page,
			'total' => $total_pages,
			'title'=> $current_page,
			'mid_size' => 2,
			'prev_text' => '<span class="pager_span"><i class="ion ion-ios-arrow-back"></i></span>',
			'next_text' => '<span class="pager_span"><i class="ion ion-ios-arrow-forward"></i></span>'
			/*'next_text' => '<i class="fa fa-angle-double-right"></i>',*/
		));
	}
}

?>