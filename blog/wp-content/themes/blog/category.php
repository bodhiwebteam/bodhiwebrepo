<?php get_header();
$cat_id = get_query_var('cat');
$category_details = get_category($cat_id);
$cat_name = $category_details->category_nicename;
if($cat_name == 'in-snap'){
?>
<div class="archieve_wrap in_snap_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="cat_page_hd gal_hder">
					<h3>Gallery</h3>
				</div>							
			</div>
		</div>		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="row archive_top">
					<div class="col-lg-12">					
					<?php
						if ( get_query_var('paged') ) {
    						$paged = get_query_var('paged');
						} else if ( get_query_var('page') ) {
    						$paged = get_query_var('page');
						} else {
    						$paged = 1;
						}
						//$args = array('category' => $category_details->term_id, 'numberposts' => -1 );
						//$myposts = get_posts( $args );
						$myposts = query_posts(array('posts_per_page' => 15, 'post_type' => 'post', 'paged' => $paged, 'cat' => $category_details->term_id, 'orderby' => 'post_date', 'order' => 'DESC', 'post_status' => 'publish') );
						$i = 1;
						foreach ( $myposts as $post ){
							$image_url = get_feat_test_image_url($post->ID);
						?>						
						<a class="in_block" href="<?php the_permalink(); ?>">
							<div class="in_block_inner">								
								<div class="archive_img">
									<img src="<?= $image_url; ?>" alt="" />
								</div>
								<h3><?= limit_words($post->post_title,12); ?></h3>
							</div>
						</a>	
						<?php
						$i += 1;
						if($i == 2){
						?>
							</div></div><div class="row"><div class="col-lg-12 s_row">
						<?php
						}					
						}	
						?>
						<div class="bd_clear"></div>
					</div>				
				</div>	
				<nav class="pagination">
					<?php pagination_bar(); ?>
				</nav>
			</div>
			
		</div>
	</div>
</div>
<?php
}
else
{
?>
<div class="archieve_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
			<?php
			if($cat_id == 65 or $cat_id == 61 or $cat_id == 55 or $cat_id == 60 or $cat_id == 54 or $cat_id == 63 or $cat_id == 64 or$cat_id == 58 or $cat_id == 57 or $cat_id == 56 or $cat_id == 52 or $cat_id == 53 or $cat_id == 62 or $cat_id == 59)
			{
			?>	
				<div class="cat_page_hd local_page_head">
					<h3><?= $category_details->name; ?></h3>
					<div class="local_selector_wrap">
						<form method="get" class="local_form" action="local.php">
                            <select class="local_selector">
                                <option value="thiruvananthapuram" <?php if($cat_name == 'thiruvananthapuram'){ echo 'selected'; } ?>>Thiruvananthapuram</option>
                                <option value="kollam" <?php if($cat_name == 'kollam'){ echo 'selected'; } ?>>Koallam</option>
                                <option value="alappuzha" <?php if($cat_name == 'alappuzha'){ echo 'selected'; } ?>>Alappuzha</option>
                                <option value="pathanamthitta" <?php if($cat_name == 'pathanamthitta'){ echo 'selected'; } ?>>Pathanamthitta</option>
                                <option value="kottayam" <?php if($cat_name == 'kottayam'){ echo 'selected'; } ?>>Kottayam</option>
                                <option value="idukki" <?php if($cat_name == 'idukki'){ echo 'selected'; } ?>>Idukki</option>
                                <option value="eranakulam" <?php if($cat_name == 'eranakulam'){ echo 'selected'; } ?>>Ernakulam</option>
                                <option value="thrissur" <?php if($cat_name == 'thrissur'){ echo 'selected'; } ?>>Thrissur</option>
                                <option value="palakkad" <?php if($cat_name == 'palakkad'){ echo 'selected'; } ?>>Palakkad</option>
                                <option value="malappuram" <?php if($cat_name == 'malappuram'){ echo 'selected'; } ?>>Malappuram</option>
                                <option value="kozhikkode" <?php if($cat_name == 'kozhikkode'){ echo 'selected'; } ?>>Kozhikkode</option>
                                <option value="wayanad" <?php if($cat_name == 'wayanad'){ echo 'selected'; } ?>>Wayanad</option>
                                <option value="kannur" <?php if($cat_name == 'kannur'){ echo 'selected'; } ?>>Kannur</option>
                                <option value="kasaragod" <?php if($cat_name == 'kasaragod'){ echo 'selected'; } ?>>Kasaragod</option>
                            </select>
                            <input id="local_toggler" value="GO" type="submit">
                        </form>
					</div>
					<div class="bd_clear"></div>
				</div>
			<?php
			}	
			else
			{
			?>
				<div class="cat_page_hd">
					<h3><?= $category_details->name; ?></h3>
				</div>
			<?php
			}
			?>				
			</div>
		</div>		
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8">
				<div class="row archive_top">
					<div class="col-lg-6 col-md-6 col-sm-6">					
					<?php
						if ( get_query_var('paged') ) {
						    $paged = get_query_var('paged');
						} else if ( get_query_var('page') ) {
						    $paged = get_query_var('page');
						} else {
						    $paged = 1;
						}
						//$args = array('category' => $category_details->term_id, 'numberposts' => 20, 'paged' => $paged, 'post_status' => 'publish', 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => 'post' );
						//$myposts = get_posts( $args );
						$myposts = query_posts(array('posts_per_page' => 20, 'post_type' => 'post', 'paged' => $paged, 'cat' => $category_details->term_id, 'orderby' => 'post_date', 'order' => 'DESC', 'post_status' => 'publish') );
						$i = 1;
						foreach ( $myposts as $post ){
							$image_url = get_feat_test_image_url($post->ID);
						?>						
						<a class="archive_block" href="<?php the_permalink(); ?>">
							<div class="archive_block_inner">								
								<div class="archive_img">
									<img src="<?= $image_url; ?>" alt="" />
								</div>
								<h3><?= limit_words($post->post_title,12); ?></h3>
							</div>
						</a>	
						<?php
						$i += 1;
						if($i == 2){
						?>
							</div><div class="col-lg-6 col-md-6 col-sm-6">
						<?php
						}
						if($i == 3){
						?>
							</div>
							</div>
							<div class="row">
								<div class="col-lg-12 secondary_row">
								
						<?php
						}
						?>					
						<?php }; 
						?>
						<div class="bd_clear"></div>
					</div>					
				</div>	
				<nav class="pagination">
					<?php pagination_bar(); ?>
				</nav>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4">
				<?php dynamic_sidebar('ad1incategorypage'); ?>
				<?php
				if($cat_name == 'edu-time' || $cat_name == 'sci-tech')
				{
				?>							
				<div class="other_news_block">
					<h3 style="margin-top: 0px;">Latest News</h3>
					<div class="other_news_inner">
						<ul>
							<?php
							$latestArgs = array(
								'category' => 7,
								'numberposts' => 5,
								'orderby' => 'post_date',
								'order' => 'DESC',
								'post_status' => 'publish'
							);
							$latestPosts = get_posts($latestArgs);
							foreach($latestPosts as $latestPost)
							{
							?>
							<li><a href="<?= get_permalink($latestPost -> ID) ?>"><?= $latestPost -> post_title; ?></a></li>
							<?php
							}
							?>
						</ul>
					</div>
				</div>
				<?php	
				}
				else
				{
				?>				
				<div class="other_news_block">
					<h3 style="margin-top: 0px;">Trending News</h3>
					<div class="other_news_inner">
						<ul>
							<?= trendingNewsArchive(); ?>
						</ul>
					</div>
				</div>				
				<?php	
				}
				?>
				<?php dynamic_sidebar('ad2incategorypage'); ?>
	             <div class="social_face">
                        <h3 class="soc_face_head">Social Face</h3>
                        <div class="social_face_tabs">
                            <!--<span class="active" data-target="#fb_soc"><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt="" /></span>
                            <span data-target="#tw_soc"><img src="<?php bloginfo('template_url'); ?>/images/tw.png" alt="" /></span>-->
                            <div class="social_face_tabs" style="display: block;">
                            	<button class="btn btn-primary active" data-target="#fb_soc">Facebook</button>
                            	<button class="btn btn-primary" data-target="#tw_soc">Twitter</button>
                            </div>
                            <div class="bd_clear"></div>
                        </div>
                        <div class="social_face_inner active" id="fb_soc">
                            <div class="fb-page" data-href="https://www.facebook.com/Suprabhaatham/" data-tabs="timeline" data-height="490" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Suprabhaatham/"><a href="https://www.facebook.com/Suprabhaatham/">Suprabhaatham Daily</a></blockquote></div></div>
                        </div>
                        <div class="social_face_inner" id="tw_soc">
                                                    <a class="twitter-timeline"  href="https://twitter.com/Suprabhaatham" data-widget-id="674570918199341056">Tweets by @Suprabhaatham</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>


                        </div>
                    </div>

				<?php dynamic_sidebar('ad3incategorypage'); ?>
			</div>
		</div>
	</div>
</div>
<?php	
}
?>
<?php get_footer(); ?>