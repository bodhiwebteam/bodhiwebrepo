<?php
get_header(); 

$current_post_id = get_the_ID();

$current_post = get_page($current_post_id);

$featured_image = get_post_featured_image_url($current_post_id);

$postUrl = get_permalink();
/*$postCategory = wp_get_post_categories($current_post_id);
//print_r($postCategory);
$postCategoryArray = [];
foreach($postCategory as $postCat){
	array_push($postCategoryArray, $postCat);
}
print_r($postCategoryArray);*/
if(in_category('editorial'))
{
?>
<div class="container inner_page_wrap">
	<div class="row">
		<div class="col-lg-12">
			<div class="editorial_single_head">
				<img src="<?php bloginfo('template_url'); ?>/images/suprabhaatham_Editorial.png" alt=""/>
				<h3>Editorial</h3>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-9 col-md-8 col-sm-8">
			<div class="news_title_block">
				<h2 class="news_title"><?= $current_post->post_title; ?></h2>
				
				<?php
				$blurb = get_post_meta($current_post->ID,'points',TRUE);
				if(!is_null($blurb)){
				?>
					<div class="blurb"><?= $blurb; ?></div>
				<?php
				}
				?>
				<div class="news_header">
					<div class="font_manager">
						<span class="news_date">
							<?php
							$date =  get_the_date( $format, $current_post->ID );
							$postTime = get_the_time( $format, $post );
							echo $date.', &nbsp;'.$postTime.' IST';
							?>
							
						</span>						
						<span class="btn-group">
							<button class="btn btn-default btn_font_control" data-role="decrease"><i class="fa fa-font"></i><sup><i class="fa fa-minus"></i></sup></button>
							<button class="btn btn-default btn_font_control" data-role="default"><i class="fa fa-font"></i></button>
							<button class="btn btn-default btn_font_control" data-role="increase"><i class="fa fa-font"></i><sup><i class="fa fa-plus"></i></sup></button>
						</span>
					</div>
					<div class="bd_clear"></div>
				</div>
				
	  		
			</div>
			<div class="news_content_block">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-4">
						<?php
				
		  		if(!is_null($featured_image)){
      
	   	 echo '<div class="editorial_img"><img src="'.$featured_image.'" alt="" /></div>';
      
	  	} ?> 
						<div class="share_wrap">
							<div class="share_head">
								<span><i class="fa fa-share-alt"></i></span><h4>Share</h4>
							</div>	
							<div class="share_ico">
								<a class="share_ico_fb" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $postUrl ?>"><i class="fa fa-facebook"></i></a>
								<a class="share_ico_tw" target="_blank" href="https://twitter.com/home?status=<?= $postUrl ?>"><i class="fa fa-twitter"></i></a>
								<a class="share_ico_gp" target="_blank" href="https://plus.google.com/share?url=<?= $postUrl ?>"><i class="fa fa-google-plus"></i></a>
							</div>	
							<div class="tag_block">
								<div class="tag_head">
									<span><i class="fa fa-tag"></i></span>TAGS
								</div>
								<div class="tags">
									<?php
									$tags = wp_get_post_tags( $current_post->ID, $args );

									foreach ( $tags as $tag ) {
										$tag_link = get_tag_link( $tag->term_id );
									?>
									<a href="<?= $tag_link ?>" title="<?= $tag->name ?>"><?= $tag->name ?></a>
									<?php	
									}
									?>
								</div>
							</div>	
							<div class="rate_block">
								<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
							</div>				
						</div>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8">
						
						<div class="news_content">
							<?=apply_filters('the_content', $current_post->post_content);?>
						</div>
					</div>
				</div>	
			</div>
			<!--<div class="share_block">
				
			</div>-->
			<div class="disqus_block">
				<?php comments_template(); ?>
			</div>
		</div>
		<div class="col-lg-3 col-md-4 col-sm-4">
			<div class="other_news_block">
				<h3>Today's Article</h3>
				<div class="other_news_inner">
					<ul>
					<?php
					$todayArgs =  array(
						'category' => 11,
						'numberposts' => 3,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_status' => 'publish'
					);
					$todayPosts = get_posts($todayArgs);
					foreach($todayPosts as $todayPost)
					{
					?>
					<li><a href="<?= get_permalink($todayPost -> ID); ?>"><?= $todayPost -> post_title; ?></a></li>
					<?php	
					}
					?>					
					</ul>
					<div class="see_lnk"><a href="<?= site_url(); ?>/category/todays-article">See All</a></div>
				</div>
			</div>
			
			<div class="other_news_block">
				<h3>Editorials</h3>
				<div class="other_news_inner">
					<ul>
					<?php
					$edtArgs =  array(
						'category' => 114,
						'post__not_in' => array($current_post_id),
						'numberposts' => 5,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_status' => 'publish'
					);
					$edtPosts = get_posts($edtArgs);
					foreach($edtPosts as $edtPost)
					{
					?>
					<li><a href="<?= get_permalink($edtPost -> ID) ?>"><?= $edtPost -> post_title; ?></a></li>
					<?php	
					}
					?>					
					</ul>
					<div class="see_lnk"><a href="<?= site_url(); ?>/category/editorial">See All</a></div>
				</div>
			</div>
			
		</div>
	</div>
	



</div>
<?php
}
elseif(in_category('todays-article'))
{
?>
<div class="container inner_page_wrap">
	<div class="row">
		<div class="col-lg-12">
			<div class="editorial_single_head">
				<img src="<?php bloginfo('template_url'); ?>/images/suprabhaatham_Editorial.png" alt=""/>
				<h3>Today's Article</h3>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-9 col-md-8 col-sm-8">
			<div class="news_title_block">
				<h2 class="news_title"><?= $current_post->post_title; ?></h2>
				
				<?php
				$blurb = get_post_meta($current_post->ID,'points',TRUE);
				if(!is_null($blurb)){
				?>
					<div class="blurb"><?= $blurb; ?></div>
				<?php
				}
				?>
				<div class="news_header">
					<div class="author_det">
						<?php
							$authorName = get_post_meta($current_post->ID,'author',TRUE);
							if($authorName != ""){
							?>
								<span><i class="fa fa-pencil-square-o"></i></span><?= $authorName; ?>
							<?php
							}
						?>
					</div>
					<div class="font_manager">
						<span class="news_date">
							<?php
							$date =  get_the_date( $format, $current_post->ID );
							$postTime = get_the_time( $format, $post );
							echo $date.', &nbsp;'.$postTime.' IST';
							?>
							
						</span>						
						<span class="btn-group">
							<button class="btn btn-default btn_font_control" data-role="decrease"><i class="fa fa-font"></i><sup><i class="fa fa-minus"></i></sup></button>
							<button class="btn btn-default btn_font_control" data-role="default"><i class="fa fa-font"></i></button>
							<button class="btn btn-default btn_font_control" data-role="increase"><i class="fa fa-font"></i><sup><i class="fa fa-plus"></i></sup></button>
						</span>
					</div>
					<div class="bd_clear"></div>
				</div>
				
	  		
			</div>
			<div class="news_content_block">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-4">
						<?php
				
		  		if(!is_null($featured_image)){
      
	   	 echo '<div class="editorial_img"><img src="'.$featured_image.'" alt="" /></div>';
      
	  	} ?> 
						<div class="share_wrap">
							<div class="share_head">
								<span><i class="fa fa-share-alt"></i></span><h4>Share</h4>
							</div>	
							<div class="share_ico">
								<a class="share_ico_fb" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $postUrl ?>"><i class="fa fa-facebook"></i></a>
								<a class="share_ico_tw" target="_blank" href="https://twitter.com/home?status=<?= $postUrl ?>"><i class="fa fa-twitter"></i></a>
								<a class="share_ico_gp" target="_blank" href="https://plus.google.com/share?url=<?= $postUrl ?>"><i class="fa fa-google-plus"></i></a>
							</div>	
							<div class="tag_block">
								<div class="tag_head">
									<span><i class="fa fa-tag"></i></span>TAGS
								</div>
								<div class="tags">
									<?php
									$tags = wp_get_post_tags( $current_post->ID, $args );

									foreach ( $tags as $tag ) {
										$tag_link = get_tag_link( $tag->term_id );
									?>
									<a href="<?= $tag_link ?>" title="<?= $tag->name ?>"><?= $tag->name ?></a>
									<?php	
									}
									?>
								</div>
							</div>	
							<div class="rate_block">
								<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
							</div>				
						</div>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8">
						<div class="news_content">
							<?=apply_filters('the_content', $current_post->post_content);?>
						</div>
					</div>
				</div>	
			</div>
			<!--<div class="share_block">
				
			</div>-->
			<div class="disqus_block">
				<?php comments_template(); ?>
			</div>
		</div>
		<div class="col-lg-3 col-md-4 col-sm-4">
			<div class="other_news_block">
				<h3>Related News</h3>
				<div class="other_news_inner">
					<ul>
					<?php
					$todayArgs =  array(
						'category' => 11,
						'post__not_in' => array($current_post_id),
						'numberposts' => 3,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_status' => 'publish'
					);
					$todayPosts = get_posts($todayArgs);
					foreach($todayPosts as $todayPost)
					{
					?>
					<li><a href="<?= get_permalink($todayPost -> ID); ?>"><?= $todayPost -> post_title; ?></a></li>
					<?php	
					}
					?>					
					</ul>
					<div class="see_lnk"><a href="<?= site_url(); ?>/category/todays-article">See All</a></div>
				</div>
			</div>
			
			<div class="other_news_block">
				<h3>Editorial</h3>
				<div class="other_news_inner">
					<ul>
					<?php
					$edtArgs =  array(
						'category' => 114,
						'numberposts' => 5,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'post_status' => 'publish'
					);
					$edtPosts = get_posts($edtArgs);
					foreach($edtPosts as $edtPost)
					{
					?>
					<li><a href="<?= get_permalink($edtPost -> ID) ?>"><?= $edtPost -> post_title; ?></a></li>
					<?php	
					}
					?>					
					</ul>
					<div class="see_lnk"><a href="<?= site_url(); ?>/category/editorial">See All</a></div>
				</div>
			</div>
			
		</div>
	</div>
	



</div>
<?php
}
elseif(in_category('in-snap'))
{
?>
<div class="in_snap_wrapper">
	<div class="container inner_page_wrap">
		<div class="row">
			<div class="col-lg-12">
				<div class="cat_page_hd gal_hder">
					<h3>Gallery</h3>
				</div>							
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-2">
				<div class="share_wrap">
					<div class="share_head">
						<span><i class="fa fa-share-alt"></i></span><h4>Share</h4>
					</div>	
					<div class="share_ico">
						<a class="share_ico_fb" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $postUrl ?>"><i class="fa fa-facebook"></i></a>
						<a class="share_ico_tw" target="_blank" href="https://twitter.com/home?status=<?= $postUrl ?>"><i class="fa fa-twitter"></i></a>
						<a class="share_ico_gp" target="_blank" href="https://plus.google.com/share?url=<?= $postUrl ?>"><i class="fa fa-google-plus"></i></a>
					</div>				
				</div>
			</div>
			<div class="col-lg-10 col-md-10 col-sm-10">
				<div class="in_snap_single_wrap" id="in_snap_pop">
					<h3 class="in_snap_title"><?= $current_post->post_title; ?></h3>
					<p class="in_snap_main_desc"><?= $current_post->post_content; ?></p>
					<div class="row" style="margin-bottom: 15px;">
						<?php
						$thumbnail_id    = get_post_thumbnail_id($current_post_id);
  						$thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
						?>
						<div class="col-lg-9 col-md-9 col-sm-9">
							<a class="in_snap_single_img" href="<?= the_post_thumbnail_url(); ?>"<?php if(($thumbnail_image && isset($thumbnail_image[0]))){ ?> data-sub-html="<h3><?= $thumbnail_image[0]->post_content ?></h3>" <?php } ?>">
								<span class="in_snap_zoomer"><i class="fa fa-search-plus"></i></span>
								<img src="<?= the_post_thumbnail_url(); ?>" alt="" />		
							</a>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3">
							<div class="in_snap_single_desc">
								<?php
									//var_dump($thumbnail_image);
									  if ($thumbnail_image && isset($thumbnail_image[0])) {	
									    ?>
									    	<h5><?= $thumbnail_image[0]->post_content; ?></h5>
									    <?php
									  }
									
								?>
							</div>
						</div>
					</div>
					<?php
						 if( class_exists('Dynamic_Featured_Image') ) {
						     global $dynamic_featured_image;
						     $featured_images = $dynamic_featured_image->get_featured_images( $current_post_id );
							 //var_dump($featured_images);
							 //$featDesc = $dynamic_featured_image -> get_image_caption_by_id($featured_images->attachment_id);
							 //echo $featDesc;
							 foreach($featured_images as $inSnapImg)
							 {
							 	$featDesc = $dynamic_featured_image -> get_image_description_by_id($inSnapImg['attachment_id']);
							 ?>
							 <div class="row" style="margin-bottom: 15px;">
							 	<div class="col-lg-9 col-md-9 col-sm-9">
									<a class="in_snap_single_img" href="<?= $inSnapImg['full']; ?>" data-sub-html="<h3><?= $featDesc ?></h3>">
										<span class="in_snap_zoomer"><i class="fa fa-search-plus"></i></span>
										<img src="<?= $inSnapImg['full']; ?>" alt="" />		
									</a>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3">
									<div class="in_snap_single_desc">
										<h5><?= $featDesc; ?></h5>
									</div>
								</div>
							 </div>
							 <?php
							 }
						    
						 }
					?>
				</div>				
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<h3 class="insnap_similar">Other Galleries</h3>
			</div>
		</div>
		<div class="row">
			<?php
				$innSnapArgs = array(
									'category' => 8,
									'post__not_in' => array($current_post_id),
									'numberposts' => 4,
									'orderby' => 'post_date',
									'order' => 'DESC',
									'post_status' => 'publish'
								);
				$innPosts = get_posts($innSnapArgs);
				foreach($innPosts as $innPost){
				?>
					<div class="col-lg-3 col-md-3 col-sm-3">
						<a class="inn_similar_lnk" href="<?= get_permalink($innPost->ID); ?>">
							<img src="<?= get_post_featured_image_url($innPost->ID); ?>"/>	
						</a>
					</div>
				<?php	
				}
			?>
		</div>
	</div>
</div>	
    
    
    <script src="<?= bloginfo('template_url'); ?>/light/lightgallery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="<?= bloginfo('template_url'); ?>/light/lg-fullscreen.min.js"></script>
    <script src="<?= bloginfo('template_url'); ?>/light/lg-thumbnail.min.js"></script>
    <script>
        jQuery(document).ready(function($){
		  //you can now use $ as your jQuery object.
		  	$('#in_snap_pop').lightGallery({
    			download: false,
                thumbnail: false,
                fullScreen: false,
                selector: '.row .col-lg-9 a'
    		});
		});
    </script>
    
    
<?php	
}
else
{
?>
<div class="container inner_page_wrap">
	<div class="row">
		<div class="col-lg-9 col-md-8 col-sm-8">
			<div class="news_title_block">
				<h2 class="news_title"><?= $current_post->post_title; ?></h2>
				
				<?php
				$blurb = get_post_meta($current_post->ID,'points',TRUE);
				if(!is_null($blurb)){
				?>
					<div class="blurb"><?= $blurb; ?></div>
				<?php
				}
				?>
				<div class="news_header">
					<div class="author_det">
						<?php
							$authorName = get_post_meta($current_post->ID,'author',TRUE);
							if($authorName != ""){
							?>
								<span><i class="fa fa-pencil-square-o"></i></span><?= $authorName; ?>
							<?php
							}
						?>
					</div>
					<div class="font_manager">
						<span class="news_date">
							<?php
							$date =  get_the_date( $format, $current_post->ID );
							$postTime = get_the_time( $format, $post );
							echo $date.', &nbsp;'.$postTime.' IST';
							?>
							
						</span>						
						<span class="btn-group">
							<button class="btn btn-default btn_font_control" data-role="decrease"><i class="fa fa-font"></i><sup><i class="fa fa-minus"></i></sup></button>
							<button class="btn btn-default btn_font_control" data-role="default"><i class="fa fa-font"></i></button>
							<button class="btn btn-default btn_font_control" data-role="increase"><i class="fa fa-font"></i><sup><i class="fa fa-plus"></i></sup></button>
						</span>
					</div>
					<div class="bd_clear"></div>
				</div>
				
	  		
			</div>
			<div class="news_content_block">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-4">
						<div class="share_wrap">
							<div class="share_head">
								<span><i class="fa fa-share-alt"></i></span><h4>Share</h4>
							</div>	
							<div class="share_ico">
								<a class="share_ico_fb" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $postUrl ?>"><i class="fa fa-facebook"></i></a>
								<a class="share_ico_tw" target="_blank" href="https://twitter.com/home?status=<?= $postUrl ?>"><i class="fa fa-twitter"></i></a>
								<a class="share_ico_gp" target="_blank" href="https://plus.google.com/share?url=<?= $postUrl ?>"><i class="fa fa-google-plus"></i></a>
							</div>	
							<div class="tag_block">
								<div class="tag_head">
									<span><i class="fa fa-tag"></i></span>TAGS
								</div>
								<div class="tags">
									<?php
									$tags = wp_get_post_tags( $current_post->ID, $args );

									foreach ( $tags as $tag ) {
										$tag_link = get_tag_link( $tag->term_id );
									?>
									<a href="<?= $tag_link ?>" title="<?= $tag->name ?>"><?= $tag->name ?></a>
									<?php	
									}
									?>
								</div>
							</div>	
							<div class="rate_block">
								<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
							</div>				
						</div>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8">
						<?php

						
		  		if(!is_null($featured_image)){
					$thumbnail_id    = get_post_thumbnail_id($current_post_id);
					$thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
					//var_dump($thumbnail_image);
					$imgDesc = $thumbnail_image[0]->post_content;
      ?>
	   	 <div class="featured_page_img">
	   	 	<img src="<?= $featured_image ?>" alt="" />
	   	 	<?php
	   	 		if(($imgDesc != '')){
					echo '<p class="img_desc">'.$imgDesc.'</p>';
				}
	   	 	?>
	   	 </div>
      	<?php
	  	} ?> 
						<div class="news_content">
							<?=apply_filters('the_content', $current_post->post_content);?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<h3 class="related_head">Related News</h3>
						<ul class="related_list">
						<?php
						$tagArray = [];
						$tags = wp_get_post_tags($current_post_id);
						if(count($tags) > 0){
							foreach($tags as $tag)
							{
								array_push($tagArray, $tag -> term_id);
							}
							$tagArgs = array('tag__in' =>$tagArray,
							'post__not_in' => array($current_post_id),
							'numberposts' => 5,
							'orderby' => 'post_date',
							'order' => 'DESC',
							'post_status' => 'publish');				
							$myposts = get_posts( $tagArgs );
							foreach ($myposts as $post) :
								setup_postdata( $post );
								?>
								<li><a href="<?= get_permalink($post->ID); ?>"><?= $post->post_title; ?></a></li>
								<?php
							endforeach;
							wp_reset_postdata();
						}
						
						
						?>
						</ul>
					</div>
				</div>
				
				
			</div>
			<!--<div class="share_block">
				
			</div>-->
			<div class="disqus_block">
				<?php //comments_template(); 
				do_shortcode('[fbcomments]'); ?>
			</div>
		</div>
		<div class="col-lg-3 col-md-4 col-sm-4">
			<?php dynamic_sidebar('ad1ininnerpage'); ?>
			<div class="other_news_block">
				<h3>Latest News</h3>
				<div class="other_news_inner">
					<ul>
					<?php
						$curCategory = get_the_category($current_post_id);
						$catArgs = array(
							'category' => 7,
							'post__not_in' => array($current_post_id),
							'numberposts' => 5,
							'orderby' => 'post_date',
							'order' => 'DESC',
							'post_status' => 'publish'
						);
						$catPosts = get_posts($catArgs);
						foreach($catPosts as $catPost)
						{
						?>
						<li><a href="<?= get_permalink($catPost -> ID) ?>"><?= $catPost -> post_title; ?></a></li>
						<?php
						}
					?>
					</ul>
				</div>
			</div>
			<?php dynamic_sidebar('ad2ininnerpage'); ?>
			<div class="trending_side">
				<h3>Trending News</h3>
				<div class="trending_side_slider_wrap">
					<ul class="trending_side_slider">
						<?= trendingNewsSlider(); ?>
					</ul>
				</div>
			</div>
			<?php dynamic_sidebar('ad3ininnerpage'); ?>
		</div>
	</div>
</div>
<?php } ?>
<?php get_footer(); ?>