<?php get_header();
$page_type = current_page_type(get_the_ID()); ?>
    <!--content-->

<?php
switch ($page_type) {
    case "njayarprabhaatham" :
        include 'pages/sunday.php';
        break;
    case "home" :
        include 'home-page.php';
        break;
    case "demo" :
        include 'pages/demo.php';
        break;
    case 'vidyaprabhaatham' :
        include 'pages/vidyaprabhaatham.php';
        break;
    case "art" :
        include 'pages/art.php';
        break;
    case "photos" :
        include 'pages/photos.php';
        break;
    case "info" :
        include 'pages/info.php';
        break;
    case "archives" :
        include 'pages/archives.php';
        break;
    case "about" :
        include 'pages/about.php';
        break;
    case "privacy" :
        include 'pages/privacy_policy.php';
        break;
    case "terms" :
        include 'pages/terms.php';
        break;
    case "contact" :
        include 'pages/contact.php';
        break;

    default:
        include 'normal-page.php';
        break;

}
?>

    <!--/content-->
<?php get_footer(); ?>