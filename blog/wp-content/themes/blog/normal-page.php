<?php

$current_post_id = get_the_ID();

$current_post = get_page($current_post_id);

$featured_image = get_post_featured_image_url($current_post_id);

?>




    



		<?php if(!is_null($featured_image)){
      
	   	 echo '<div class="header-image" style="background-image:url('.$featured_image.');"></div>';
      
	  	} ?>  
	  	<div class="container">
	  		<div class="row">
	  			<div class="col-lg-12">
	  				<h2 class="main_head text-center"><?=  $current_post->post_title ?></h2>
	  				<div class="page_content">
	  					<?=apply_filters('the_content', $current_post->post_content);?>
	  				</div>
	  			</div>
	  		</div>
	  	</div>
	

      <!--bottom content--> 
