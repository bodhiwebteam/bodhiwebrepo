<?php
header( "refresh:3;url=career.html"); 
?>
<!DOCTYPE HTML>
<html lang="">

<head>
<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<style>
        body{
            background-color: #f5f7fa;
        }
        .panel-heading h4{
            margin: 0px;
        }
        .panel{
            max-width: 290px;
            margin: 50px auto 0px auto;
        }
	</style>
</head>
<body>
<?php

require_once('PHPMailer/class.phpmailer.php');


if($_POST) 
{
	
	$fname		=	trim(ucfirst(strtolower($_POST['fname'])));
	$lname		=	trim(ucfirst(strtolower($_POST['lname'])));
	$phone		=	trim($_POST['phone']);
	$email		=	$_POST['email'];
	$post		=	$_POST['post'];
	$profile	=	$_POST['profile'];
	
	$file		=	$_FILES["file"]["tmp_name"];
	$filename	=	$_FILES["file"]["name"];
	
	$emailTo = 'bodhicareer0721@gmail.com';
	$subject= "Mail from bodhi websit Career page ";
	$bodytext = " First Name : ".$fname."\r\n Last Name : ".$lname."\r\n Phone : ".$phone."\r\n Email : ".$email."\r\n Post Applied For : ".$post."\r\n Brief Profile : ".$profile."\r\n";
	 
	$email = new PHPMailer();
	$email->From      = "career@bodhiinfo.com";
	$email->FromName  = $fname;
	$email->Subject   = $subject;
	$email->Body      = $bodytext;
	$email->AddAddress($emailTo);

	$file_to_attach = $file;

	$email->AddAttachment( $file_to_attach , $filename );

	if($email->Send())
	{
		echo '<div class="panel panel-primary"><div class="panel-heading"><h4>Success</h4></div><div class="panel-body">Message has been sent.You will be directed back soon.</div></div>';
	}
	else
	{
		echo '<div class="panel panel-danger"><div class="panel-heading"><h4 class="text-danger">Success</h4></div><div class="panel-body">Message could not be sent. Please try again. You will be directed back soon.</div></div>';
	}
}

?>
</body>
</html>