<?php
// auto-generated by sfSecurityConfigHandler
// date: 2017/02/27 11:17:48
$this->security = array (
  'ldap' => 
  array (
    'is_secure' => false,
  ),
  'restorepassword' => 
  array (
    'is_secure' => false,
  ),
  'all' => 
  array (
    'is_secure' => true,
  ),
);
