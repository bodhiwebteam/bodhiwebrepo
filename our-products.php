<?php
// Merchant key here as provided by Payu
$MERCHANT_KEY = "QXzGS3ja";

// Merchant Salt as provided by Payu
$SALT = "xg8slmDcy0";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";

$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
   
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bodhi Info Solutions Pvt Ltd</title>
    <meta name="description" content="">
    <link rel="icon" href="images/fav_ico.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="css/vegas.css">
    <link rel="stylesheet" type="text/css" href="css/simpletextrotator.css">
    <link rel="stylesheet" type="text/css" href="css/map_style.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="js/jQuerySimpleCounter.js"></script>
    <script src="js/vegas.min.js"></script>
    <script src="js/jquery.simple-text-rotator.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="js/script.js"></script>
     <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
    <style>
        .bd_modal .modal-content{
            border-radius: 0px;
        }
        .bd_modal .modal-header{
            background-color: #008DCE;
        }
        .bd_modal .modal-header h4{
            color: #FFF;
        }
    </style>
</head>

<body onload="submitPayuForm()">
<div class="career_banner_wrap">
    <div class="container">
        <h1>Products</h1>
        <h4>Bodhi Owns these.</h4> </div>
</div>
<div class="header">
    <div class="container">
        <div class="logo">
            <a href="http://www.bodhiinfo.com"><img src="images/logo.png" alt="Bodhi"></a>
        </div>
        <div class="nav_wrap"><span class="nav_trigger"><i class="fa fa-bars"></i></span>
            <ul class="navigation">
                <li> <a href="index.html"> home <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="about.html"> about <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="service.html"> services <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="products.html" class="active"> products <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="portfolio.html"> portfolio <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="career.html"> career <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="team.html"> team <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="http://bodhiinfo.com/blog"> blog <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="social-commitment.html"> social commitment <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
                <li> <a href="contact.html"> contact <span class="nav_border nav_border_top"></span> <span class="nav_border nav_border_right"></span> <span class="nav_border nav_border_bottom"></span> <span class="nav_border nav_border_left"></span> </a> </li>
            </ul>
        </div>
    </div>
</div>
<div class="product_wrap">
    <div class="container">
        <div class="product_box">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_img"> <img src="images/lapiz.jpg" alt="lapiz"> </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_description">
                        <div class="product_logo"> <img src="images/lapiz_logo.png" alt="lpzlogo"> </div>
                        <h5>College &amp; School management software solution</h5>
                        <p>This is a world class comprehensive web-based College &amp; School management software solution to assist parents, administrative and management with daily activities of the institution and the students. This application helps the parents and teachers to track the performance and progress of the students. Parents and teachers can login to their respective accounts to view the attendance, academic scores and the extracurricular activities of the students. The school can announce and notify the parents and management through the application. </p>
                        <div class="clear-fix">
                            <div class="product_cat"> <img src="images/web_app.png" alt="webapp">
                                <p>Web
                                    <br>Application</p>
                            </div>
                            <a href="javascript:void(0)"><i class="fa fa-rupee"></i> Rs. 699/Month</a>
                            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#lapiz_pop" style="padding: 10px 30px;">Buy Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_box">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_img"> <img src="images/ebzilon.png" alt="ebzilon"> </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_description ebzilon_des">
                        <div class="product_logo"> <img src="images/ebzilon_logo.png" alt="ebzlnlogo"> </div>
                        <p>An inventory accounting software that meets all the business and management operations. It effortlessly manages invoices, accounts, bank transactions, payments and receipts. It maintains the billing of all expenses, consultant invoices, and monthly invoices, in accordance with standard procedures, cash receipts team member to reconcile variances that occur in the application of cash.</p>
                        <div class="clear-fix">
                            <div class="product_cat"> <img src="images/web_app.png" alt="webapp">
                                <p>Desktop
                                    <br>Application</p>
                            </div>
                            <a href="javascript:void(0)"><i class="fa fa-rupee"></i> Rs. 699/Month</a>
                            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#lapiz_pop" style="padding: 10px 30px;">Buy Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_box">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_img"> <img src="images/ent_nadu.png" alt="entnadu"> </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_description ent_nadu_des">
                        <div class="product_logo"> <img src="images/ent_nadu_logo.png" alt="entlogo"> </div>
                        <p>An efficient social service application to bring together and coordinate the members and the activities of any social group. You can market your business or avail any facility near you. You can get all the emergency numbers and other important contacts. The Application stores the details and contacts of the blood donors, important and socially concerned people and more. Also display Ads, events and news, search for a job or post a job vacancy through the app. In short the application is a network which can connect a whole city.</p>
                        <div class="clear-fix">
                            <div class="product_cat"> <img src="images/web_app.png" alt="webapp">
                                <p>Mobile
                                    <br>Application</p>
                            </div>
                            <a href="javascript:void(0)"><i class="fa fa-rupee"></i> Rs. 699/Month</a>
                            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#lapiz_pop" style="padding: 10px 30px;">Buy Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_box">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_img"> <img src="images/abc_travels.png" alt="travlmngmt"> </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_description ent_nadu_des">
                        <!--<div class="product_logo"> <img src="images/ent_nadu_logo.png" alt=""> </div>-->
                        <h5>Travel Management</h5>
                        <p>Travel management is a web application in which a travel agency can manage and control the functions of their respective branches. It helps admin to manage a company travel policy and ensures control over functions. Also it will help agencies to provide the day to day travel service.</p>
                        <div class="clear-fix">
                            <div class="product_cat"> <img src="images/web_app.png" alt="webapp">
                                <p>Web
                                    <br>Application</p>
                            </div>
                            <a href="javascript:void(0)"><i class="fa fa-rupee"></i> Rs. 699/Month</a>
                            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#lapiz_pop" style="padding: 10px 30px;">Buy Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_box">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_img"> <img src="images/ilaj_app.jpg" alt="ilag"> </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="product_description ent_nadu_des">
                        <!--<div class="product_logo"> <img src="images/ent_nadu_logo.png" alt=""> </div>-->
                        <h5>Hospital Management</h5>
                        <p>The operational challenge of hospitals multiplies each year with changes in patient expectation and with the increase in competition. Through this Desktop Application it makes easier for the management to manage the operations of hospital.</p>
                        <div class="clear-fix">
                            <div class="product_cat"> <img src="images/web_app.png" alt="webapp">
                                <p>Desktop
                                    <br>Application</p>
                            </div>
                            <a href="javascript:void(0)"><i class="fa fa-rupee"></i> Rs. 699/Month</a>
                            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#lapiz_pop" style="padding: 10px 30px;">Buy Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_col1">
                    <div class="footer_logo"> <img src="images/footer_logo.png" alt="bodhi_ft"> </div>
                    <div class="footer_social"> <a target="_blank" href="https://www.facebook.com/pages/Bodhi-info-solutions/293809530773903"><span><i class="fa fa-facebook-f"></i></span></a> <a target="_blank" href="https://www.linkedin.com/company/bodhi-info-solutions"><span><i class="fa fa-linkedin"></i></span></a> <a target="_blank" href="https://plus.google.com/115280885518776516760/about?hl=en"><span><i class="fa fa-google-plus"></i></span></a> </div>
                    <div class="footer_broch_lnks"><a href="bodhi_brochure.pdf" target="_blank"><i class="ion ion-android-download"></i>Download Our Brochure</a></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_col2">
                    <div class="footer_projection">
                        <ul>
                            <li>
                                <div class="in_flx"><span id="count-test-2" class="demo"></span>
                                    <h2>+</h2></div>
                                <h4>Developed Projects</h4> </li>
                            <li>
                                <div class="in_flx"><span id="count-test-3" class="demo"></span>
                                    <h2>%</h2></div>
                                <h4>Dedicated Team</h4> </li>
                            <li>
                                <div class="in_flx"><span id="count-test-4" class="demo"></span>
                                    <h2>+</h2></div>
                                <h4>Countries</h4> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="footer_contact">
                    <h4>contact</h4>
                    <p>Kuniyil Kavu Junction,
                        <br/> Jafferkhan Colony Road,
                        <br>Calicut - 673001</p>
                    <hr>
                    <div class="email_row"><span><i class="fa fa-envelope"></i></span><a href="mailto:mail@bodhiinfo.com"> mail@bodhiinfo.com </a>
                        <br>
                    </div>
                    <br>
                    <div class="number_row"><span><i class="fa fa-phone"></i></span><a href="tel:+9104954019587"> 0495 4019587 </a>
                        <br>
                    </div>
                    <div class="number_row"><span><i class="fa fa-mobile"></i></span><a href="tel:+918086450007"> 08086 450007</a></div>
                </div>
            </div>
        </div>
        <hr>
        <p class="copy">Copyright © 2017 Bodhi. All Rights Reserved.</p>
    </div>
</footer>

<!-- Modal -->
<div id="lapiz_pop" class="modal fade bd_modal" role="dialog">
    <div class="modal-dialog">
      
        <form action="<?php echo $action; ?>" method="post" name="payuForm">
	    	<input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
		<input type="hidden" name="hash" value="<?php echo $hash ?>"/>
		<input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
     		<input type="hidden" name="amount" value="1" />
     		
     		
      
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Please fill the form</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>First Name</label>
                        <input class="form-control" type="text" name="firstname" id="firstname" placeholder="Enter your first name" required value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>">
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input class="form-control" type="text" name="lastName" placeholder="Enter your last name" required>
                    </div>
                    <div class="form-group">
                        <label>Email ID</label>
                        <input class="form-control" type="email" name="email" id="email" placeholder="Enter your email ID here" required value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>">
                    </div>
                    <div class="form-group">
                        <label>Contact No</label>
                        <input class="form-control" type="text" name="phone" placeholder="Contact No" required value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>">
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" name="productinfo" placeholder="productinfo" required><?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?></textarea>
                    </div>
                    <div class="form-group">
                    	<input type="hidden" name="surl" value="http://bodhiinfo.com/payment/success.php" size="64" />
     			<input type="hidden" name="furl" value="http://bodhiinfo.com/payment/failure.php" size="64" />
     			<input type="hidden" name="service_provider" value="payu_paisa" size="64" />
     		    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" value="Check Out" />
                </div>
            </div>
        </form>

    </div>
</div>
</body>

</html>